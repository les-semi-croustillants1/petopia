### Table of content
#### [Food](#i-food)
#### [Tips](#ii-tips)
#### [Adopt](#iii-adopt)
#### [Origin](#iv-origin)


<center>
<img src="/images/poisson-clown.jpg" alt="drawing" width="300" />
</center>


## I - Food

Plankton represents the main source of food for clownfish. The study of the stomach contents of captured specimens generally reveals copepods and tunicate larvae. Clownfish also consume a significant amount of algae grazed on nearby reefs or gobbled up in open water.

Sometimes, in aquariums, a specimen that is presented with a large food item that it cannot fully consume will bring it back to the safety of its territory, the anemone. Anemones are carnivorous and it is ultimately the host that eats the food it was meant to protect. The fish does not intentionally feed the anemone even though it may appear so to an observer. In the wild this behavior is absent. Food is consumed directly where it is found because clownfish do not encounter large food items.

## II - Tips

Two species are, by their size, adapted to a life in an aquarium: the best known, the three-banded clownfish (Amphiprion ocellaris) is easily recognized by its very bright orange color and its three vertical white bands. Another star of the family is the Pacific clownfish (Amphiprion percula) which can be distinguished by the thin black stripes delimiting the three white stripes on its body. Among the 29 species listed, we can also mention the fire clownfish (Amphiprion melanopus) dominated by the orange color and a vertical white stripe bordered with black behind the eye. Amphiprion measure between 7 and 15 centimeters long.

## III - Adopt

The clownfish is a gregarious animal that moves in group. It is therefore necessary to adopt several of them and never let them live alone. Keep at least one couple if your tank is small, to avoid depression and premature death, and choose a small group if you are lucky enough to have a large aquarium.
Clownfish need salt water and a sea anemone to live. However, the breeding of anemones in an aquarium is very complex. It is therefore advised to reserve their adoption to passionate, experienced and well-equipped aquarists.


## IV - Origin

The term clownfish designates two species of the genus Amphiprion from the family Pomacentridae: Amphiprion percula, which is the Pacific clownfish, and Amphiprion ocellaris, which is the three-banded one. In general, all species of amphiprion are called clownfish.

This fish is native to the Pacific and Indian oceans and lives in salt water. It measures between 7 and 15 cm on average when adult. Although the cartoon "Nemo's World" made it famous in red striped with white and black, the clownfish can present a wider range of colors, like black, orange, red and yellow. In addition, it can be striped or spotted depending on the individual.