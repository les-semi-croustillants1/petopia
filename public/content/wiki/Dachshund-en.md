### Table of content
#### [Food](#i-food)
#### [Tips](#ii-tips)
#### [Adopt](#iii-adopt)
#### [Origin](#iv-origin)
--------------------------

<center>
<img src="/images/teckel.jpg" alt="drawing" width="300" />
</center>

## I - Food

This dog is very short on legs: its curved limbs of the chondrodystrophic type can pose a problem for the Dachshund. Its joints are weakened by the atypical shape of its legs. It is therefore necessary to protect its joint capital by using its food.
The Dachshund has a tendency to gain weight: this small dog is indeed prone to overweight. But extra pounds are really not recommended for the Dachshund, because due to the elongated shape of his body, his spine can quickly become overweight. 

## II - Tips

The Dachshund is a dog used for hunting, and only 1/3 of the births are destined to be a pet. It likes the countryside and walks. This dog is suitable for children because it knows the difference between adults and children and will be much more lenient with the latter. It will not show aggressiveness towards the child but will be less indulgent towards the adult. The Dachshund is a dog with a dominant tendency, it is necessary to know how to put it in its place in the family.

## III - Adopt

The Dachshund is a dog that can be exclusive with its master, especially the short-haired Dachshund. Indeed, he is always looking for his master's mistake to become the leader. It is considered a dog of superior intelligence that does not let any mistake pass because it is a dominant in the soul. It is joyful, lively and playful, but should not be placed in too permissive hands.


## IV - Origin

At the origin of this breed and other similar breeds, there was a selection on the aptitude for hunting. It is likely that the Dachshund's usefulness in hunting in a den was discovered at the same time in different parts of the world. The Dachshund is considered a recent breed, dating back to the 18th - 19th century: its ancestor is the German Dachshund, its close cousin. Its more recent history begins in Germany and more precisely in Bavaria, where the first breedings started. In England too, the Dachshund is bred with great passion, either as a hunting dog or as a companion dog, but with a different standard.