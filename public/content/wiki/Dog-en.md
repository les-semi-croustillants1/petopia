### Table of content
#### [Food](#i-food)
#### [Tips](#ii-tips)
#### [Adopt](#iii-adopt)
#### [Notes](#iv-notes)
--------------------------

<center>
<img src="/images/chien.jpg" alt="drawing" width="300" />
</center>

## I - Food

The dog is a carnivore who can also adapt his diet to eat only kibble.

## II - Tips

The dog does not generally need to wash itself. If you do it is only to not dirty your apartment or your house. 

## III - Adopt

A dog does not like to be alone. He needs to share your daily life. A dog is a great companion for your children, but it is not a toy. Dogs, especially the larger, more muscular ones (Newfoundland, Boxer, etc.) and the livelier ones (Pyrenean Shepherd, terriers, etc.) need space and muscular activity: play, work, etc. If there is no garden where the animal can stay as long as it wants, it needs to go out at least four times a day (once every six hours or so) for about 20 minutes, to "exercise".

## IV - Notes

The study of dogs and dog breeds is called cynology. It includes approaches, techniques, philosophies and various tools used for dog training and good behavior of dogs as well as their biological selection. Depending on the breed and the genetic variations from one individual to another, dogs can have a very varied coat. 

