### Sommaire
#### [La nourriture](#i-la-nourriture)
#### [Conseils](#ii-conseils)
#### [Adopter](#iii-adopter)
#### [Origine](#iv-origine)
--------------------------

<center>
<img src="/images/teckel.jpg" alt="drawing" width="300" />
</center>

## I - La Nourriture

Ce chien est très court sur patte : ses membres courbés de type chondrodystrophe peuvent poser problème chez le Teckel. Ses articulations sont fragilisées par la forme atypique de ses pattes. Il faudra donc protéger son capital articulaire en utilisant son alimentation.
Le Teckel a tendance à prendre du poids : ce petit chien est en effet sujet à l’embonpoint. Mais les kilos en trop ne sont vraiment pas recommandés chez le Teckel, car en raison de la forme allongée de son corps, sa colonne vertébrale peut rapidement souffrir d’une surcharge pondérale. 

## II - Conseils

Le Teckel est un chien utilisé pour la chasse, et seulement 1/3 des naissances destine ce chien à être un animal de compagnie. Il aime la campagne et les promenades. Ce chien convient aux enfants car il sait faire la différence entre adulte et enfant et sera beaucoup plus indulgent avec ce dernier. Il ne manifestera pas d'agressivité envers l'enfant mais sera moins indulgent envers l'adulte. le Teckel est un chien à tendance dominante, il faut savoir le mettre à sa place dans la famille.

## III - Adopter

C'est un chien qui peut être exclusif avec son maître, en particulier le Teckel à poils ras. En effet, il est toujours à la recherche de l'erreur de son maître pour devenir le chef. On considère que c'est un chien d'une intelligence supérieure qui ne laisse passer aucune erreur car c'est un dominant dans l'âme. Il est joyeux, vif et joueur, mais ne doit pas être placé entre des mains trop permissives


## IV - Origine

À l’origine de cette race et d’autres races semblables, il y a eu un travail de sélection sur l'aptitude à la chasse. Il est probable que l’utilité du Teckel dans la chasse en terrier ait été découverte en même temps dans différentes parties du monde. On considère que le Teckel est une race récente qui date du 18 - 19ème siècle : son ancêtre est le Basset allemand, son proche cousin. Son histoire plus récente commence en revanche en Allemagne et plus précisément en Bavière, où ont débuté les premiers élevages. En Angleterre aussi, le Teckel est élevé avec beaucoup de passion, soit comme chien de chasse soit comme chien de compagnie, mais avec un standard différent.