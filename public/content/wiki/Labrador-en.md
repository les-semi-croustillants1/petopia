### Table of content
#### [Food](#i-food)
#### [Tips](#ii-tips)
#### [Adopt](#iii-adopt)
#### [Origin](#iv-origin)
--------------------------

<center>
<img src="/images/labrador.jpg" alt="drawing" width="300" />
</center>

## I - Food

This is a very voracious dog that tends to gain weight quite quickly. Be careful not to give him too much to eat or food that is too fatty.

## II - Tips

The Labrador needs to exercise regularly in order not to get fat, because as said before he can get fat quickly.

## III - Adopt

A very docile, playful and intelligent breed with spectacular looks, the Labrador is officially approved as a water dog and retriever. However, its exceptional character has made it a much sought-after companion and working dog, especially as a guide dog for the blind, or as a drug or explosives detector for law enforcement and customs.


## IV - Origin

The origin of the Labrador has many points in common with that of the Newfoundland and it is quite difficult to distinguish the two original forms. In many texts of the 19th century, the terms "Newfoundland" and "Labrador" are used without distinction to refer to the water dogs of the Canadian coasts. The ancestor of the Labrador seems to be the "St. John's Dog", a smaller version of the Newfoundland that developed in Canada at about the same time as this dog. The Cão de Castro Laboreiro probably also contributed to the formation of the Labrador. The breed then spread widely to Great Britain and it is this country that obtained the paternity of the breed.