### Sommaire
#### [La nourriture](#i-la-nourriture)
#### [Conseils](#ii-conseils)
#### [Adopter](#iii-adopter)
#### [Notes](#iv-notes)
--------------------------

<center>
<img src="/images/poisson.png" alt="drawing" width="300" />
</center>

## I - La Nourriture

Les poissons peuvent être omnivores, carnivore, herbivore, il faudra vous renseigner plus en détail sur l'espèce. 

## II - Conseils

N'hésiter à vous renseigner et à approfondir vos connaissances avant d'en adapter un.

## III - Adopter

Se renseigner en détails sur les prix d'installations et d'entretient qui peuvent varié en fonction de l'espèce.
Ne pas les relacher dans la nature, ils pourraient détruire tous les écosystèmes environants.


## IV - Notes

Un poisson typique est « à sang froid », il possède un corps allongé lui permettant de nager rapidement, il extrait le dioxygène de l'eau en utilisant ses branchies ou un organe respiratoire annexe lui permettant de respirer le dioxygène atmosphérique. Il possède deux paires de nageoires, les nageoires pelviennes et latérales, habituellement une ou deux (plus rarement trois) nageoires dorsales, une nageoire anale et une nageoire caudale. Il possède une double mâchoire pour les gnathostomes et simple pour les agnathes, il possède une peau généralement recouverte d'écailles, ovipare, il pond des œufs et la fécondation peut être interne ou externe.