### Table of content
#### [Food](#i-food)
#### [Tips](#ii-tips)
#### [Adopt](#iii-adopt)
#### [Notes](#iv-notes)
--------------------------

<center>
<img src="/images/poisson.png" alt="drawing" width="300" />
</center>

## I - Food

Fish can be omnivorous, carnivorous, herbivorous, you will have to get more information about the species. 

## II - Tips

Do not hesitate to inform you and to deepen your knowledge before adapting one.

## III - Adopt

Get detailed information on the prices of installations and maintenance which can vary depending on the species.
Do not release them in the wild, they could destroy all the surrounding ecosystems.


## IV - Notes

A typical fish is "cold-blooded", it has an elongated body allowing it to swim quickly, it extracts oxygen from the water by using its gills or an attached respiratory organ allowing it to breathe atmospheric oxygen. It has two pairs of fins, the pelvic and lateral fins, usually one or two (more rarely three) dorsal fins, an anal fin and a caudal fin. It has a double jaw for the gnathostomes and simple for the agnathes, it has a skin generally covered with scales, oviparous, it lays eggs and the fertilization can be internal or external.