### Sommaire
#### [La nourriture](#i-la-nourriture)
#### [Conseils](#ii-conseils)
#### [Adopter](#iii-adopter)
#### [Origine](#iv-origine)


<center>
<img src="/images/persan.jpg" alt="drawing" width="300" />
</center>

## I - La Nourriture

La qualité des nutriments dans la nourriture, la facilité de préhension et de mastication des aliments avec son museau très court, l’effet sur sa dentition, la quantité à lui donner… sont autant de points à surveiller afin de maintenir une bonne santé physique, une fourrure douce et plus facile d’entretien et une cote de chair idéale. Favoriser la consommation d’eau est aussi important. Finalement, l’utilisation de bols interactifs est recommandée pour ralentir l’ingestion de la nourriture, stimuler physiquement et mentalement le chat et possiblement limiter sa suralimentation et l’embonpoint qui y est associé.

## II - Conseils

La robe est un critère excessivement important pour la race. Virtuellement, toutes les couleurs sont possibles, mais certaines associations félines en distinguent quelques-unes au point de les élever au statut de race.
On appelle Persan Classique celui dont la robe est unicolore, particolore, écaille de tortue, tabby ou smoke. Chaque couleur définit une variété particulière : le Persan Bleu, le Persan Écaille et Blanc, le Persan Black Smoke… Pour chaque variété, il y a des restrictions pour la couleur des yeux.

## III - Adopter

Le Persan est calme et placide, il aime la paix et la douceur du farniente ; il passe une grande partie de la journée à somnoler, sans se laisser distraire par aucun mouvement ni bruit. Le calme qui le caractérise ne fait pas pour autant de lui un « mollasson », il révèle au contraire souvent un tempérament fort et aristocratique. Même s’il ne le montre pas, parce qu’il paraît distant et sur son quant-à-soi, il a un grand besoin d’affection qu’il redonne avec fidélité et sympathie. En outre, conscient de sa beauté, il aime en faire effet : il veut être admiré et complimenté.


## IV - Origine

La race des Persans est artificielle car ces chats proviennent d’une sélection longue et soigneuse pratiquée par l’homme à partir du XIXe siècle. Son ancêtre est l’Angora turc, originaire de Turquie. Dès le XVIIe siècle, l’explorateur Pietro della Valle tomba amoureux de ce chat à la longue robe et l’importa d’Asie Mineure pour le faire connaître en Europe. En 1880, d’autres Angoras furent amenés en France et en Angleterre où furent sélectionnés les sujets les plus courts et les plus ronds dont proviennent les premiers Persans ; leur beauté leur procura un énorme succès : ils se répandirent non seulement en Europe mais aussi aux États-Unis. L’aspect des Persans actuels est différent de celui de leurs ancêtres car la sélection s’est affinée afin d’obtenir des chats plus poilus et plus massifs.