### Sommaire
#### [La nourriture](#i-la-nourriture)
#### [Conseils](#ii-conseils)
#### [Adopter](#iii-adopter)
#### [Notes](#iv-notes)
--------------------------

<center>
<img src="/images/chat.jpg" alt="drawing" width="300" />
</center>

## I - La Nourriture

Le chat est un carnivore. Il est donc tout à fait capable de chasser de lui-même des souris et autres petits rongeurs. Une particularité alimentaire du chat est de manger de l'herbe. Ce n'est pas pour se nourrir mais pour se forcer à recracher les poils qu'il absorbe durant sa toilette.

## II - Conseils

Le chat sait se toiletter tout seul, il n'est pas nécéssaire de le laver. Le chat est un animal qui possède et protège son territoire. Il est donc normal qu'il se batte avec d'autres chats de temps en temps. Il ne faut donc pas l'en empêcher car cela ferai de votre chat un animal soumis aux autres et ne possédant pas de territoire, même chez lui. Mais bien sur, vous pouvez désinfecter les plaies après ses combats.

## III - Adopter

Un chat d'appartement ne nécéssite pas beaucoup d'espace mais doit néanmoins pouvoir se défouler sur des arbres à chats. Si vous possédez un jardin le chat s'amusera dehors comme il veut. Une chatière au niveau de la porte pour lui permettre de sortir est un gros plus pour vous epargner de vous lever lorsque vous serez sur la canapé devant un film et qu'il souhaitera aller dehors.


## IV - Notes

Il est l’un des principaux animaux de compagnie et compte aujourd’hui une cinquantaine de races différentes reconnues par les instances de certification. Dans de très nombreux pays, le chat entre dans le cadre de la législation sur les carnivores domestiques à l’instar du chien et du furet. Essentiellement territorial, le chat est un prédateur de petites proies comme les rongeurs ou les oiseaux. Les chats ont diverses vocalisations dont les ronronnements, les miaulements, les feulements ou les grognements, bien qu’ils communiquent principalement par des positions faciales et corporelles et des phéromones 