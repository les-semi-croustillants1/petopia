### Sommaire
#### [La nourriture](#i-la-nourriture)
#### [Conseils](#ii-conseils)
#### [Adopter](#iii-adopter)
#### [Origine](#iv-origine)


<center>
<img src="/images/poisson-clown.jpg" alt="drawing" width="300" />
</center>

## I - La Nourriture

Le plancton représente la source principale de l'alimentation des poissons-clowns. L'étude du contenu stomacal de spécimens capturés révèle généralement des copépodes et des larves de tuniciers. Le poisson Clown consomme également une quantité importante d'algues broutées sur les récifs proches ou gobées en pleine eau.

Parfois, en aquarium, un spécimen à qui l'on présente un aliment de grande taille qu'il ne peut pas consommer entièrement le rapporte pour le mettre en sécurité sur son territoire, c'est-à-dire l'anémone. Les anémones sont carnivores et c'est finalement l'hôte qui mange les aliments qu'elle était censée protéger. Le poisson ne nourrit pas intentionnellement l'anémone même si cela peut sembler être le cas pour un observateur. À l'état sauvage ce comportement est absent. Les aliments sont consommés directement à l'endroit où ils sont trouvés car les poissons-clowns ne rencontrent pas d'aliments de taille importante.

## II - Conseils

Deux espèces sont, par leur taille, adaptées à une vie en aquarium : le plus connu, le poisson-clown à trois bandes (Amphiprion ocellaris) se reconnait facilement par sa couleur orangée très lumineuse et par ses trois bandes blanches verticales. Autre vedette de la famille : le poisson-clown du Pacifique (Amphiprion percula) qui se distingue par les fines bandes noires délimitant les trois bandes blanches rayant son corps. Parmi les 29 espèces recensées, on peut aussi citer le poisson clown de feu (Amphiprion melanopus) dominé par la couleur orange et une bande blanche verticale bordée de noir derrière l'œil. Les amphiprions mesurent entre 7 et 15 centimètres de long.

## III - Adopter

Le poisson-clown est un animal grégaire qui se déplace en banc. Il est donc nécessaire d’en adopter plusieurs et de ne jamais les laisser vivre seuls. Élevez-en au minimum un couple si votre bac est petit, afin d’éviter la dépression et le décès prématuré, et choisissez un petit groupe si vous avez la chance d’avoir un grand aquarium.
Les poissons-clowns ont besoin d’eau salée et d’une anémone de mer pour vivre. Or, l’élevage des anémones en aquarium est très complexe. Il est donc conseillé de réserver leur adoption à des aquariophiles passionnés, expérimentés et bien équipés.


## IV - Origine

Le terme de poisson-clown désigne deux espèces du genre Amphiprion issus de la famille des Pomacentridae : Amphiprion percula, qui est le poisson-clown du Pacifique, et Amphiprion ocellaris, qui est celui à trois bandes. D’ordre général, toutes les espèces d’amphiprions sont appelées poissons-clown.

Ce poisson originaire des océans pacifique et indien, qui vit en eau de mer, mesure entre 7 et 15 cm en moyenne à l’âge adulte. Bien que le dessin animé "Le monde de Nemo" l’ait rendu célèbre en rouge rayé de blanc et de noir, le poisson-clown peut présenter un plus grand panel de couleurs, comme le noir, l’orange, le rouge et le jaune. En outre, il peut être rayé ou tacheté selon les individus.