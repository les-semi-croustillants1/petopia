### Sommaire
#### [La nourriture](#i-la-nourriture)
#### [Conseils](#ii-conseils)
#### [Adopter](#iii-adopter)
#### [Origine](#iv-origine)


<center>
<img src="/images/siamois.jpg" alt="drawing" width="300" />
</center>

## I - La Nourriture

Le siamois est un chat de type longiligne, de taille moyenne, au corps svelte et bien musclé. Le chat doit être en bonne condition physique. Il faut donc veiller à ne pas le sous-alimenter en voulant à tout prix à le garder en bonne santée.

## II - Conseils

Des études basées sur des données provenant de compagnies d'assurance suédoises, ont démontré que les Siamois et ses races dérivées ont un taux élevé de mortalité comparé aux autres races. La majorité des décès sont dus à des néoplasmes et à des tumeurs mammaires. Les Siamois ont également un fort taux de morbidité. L'espèce est plus soumise aux problèmes gastro-intestinaux, mais peut être aussi fragilisée par des maladies des voies urinaires inférieures. Il faut donc penser à l'ammener régulièrement chez le vétérinaire.

## III - Adopter

Il vient dès qu'on l'appelle, comprend vite les interdictions et a tendance à suivre son compagnon humain spontanément et dans de nombreux cas systématiquement. On peut aisément lui apprendre à marcher en laisse et l'emmener partout, même en vacances, car, contrairement à la majorité des autres chats, il développe un attachement à l'humain plutôt qu'à un territoire. Très attaché à son maître, il supporte mal la solitude. Il faut donc que son maître soit très présent.


## IV - Origine

L'origine se trouve dans le Sud-Est de l'Asie : la race pourrait descendre des chats sacrés des temples de Siam (d'où leur nom). Ces chats étaient déjà représentés dans certains manuscrits retrouvés à Ayutthaya (ancienne capitale du Siam) et datant de 1350. Un naturaliste allemand a également décrit des chats aux extrémités foncées dans le courant du xixe siècle. Ils étaient également dégustés pour la qualité de leur viande.