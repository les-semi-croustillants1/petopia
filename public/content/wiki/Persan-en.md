### Table of content
#### [Food](#i-food)
#### [Tips](#ii-tips)
#### [Adopt](#iii-adopt)
#### [Origin](#iv-origin)


<center>
<img src="/images/persan.jpg" alt="drawing" width="300" />
</center>

## I - Food

The quality of the nutrients in the food, the ease of prehension and chewing of the food with its very short muzzle, the effect on its dentition, the quantity to be given to it... are all points to be monitored in order to maintain a good physical health, a soft and easier to maintain coat and an ideal flesh rating. Encouraging water consumption is also important. Finally, the use of interactive bowls is recommended to slow down the ingestion of food, to stimulate the cat physically and mentally and possibly to limit its overeating and the associated overweight.

## II - Tips

The coat is an extremely important criterion for the breed. Virtually all colors are possible, but some feline associations distinguish some to the point of raising them to the status of breed.
A Classic Persian is one whose coat is solid color, particolored, tortoiseshell, tabby or smoke. Each color defines a particular variety: the Blue Persian, the Tortoiseshell and White Persian, the Black Smoke Persian... For each variety, there are restrictions for the color of the eyes.

## III - Adopt

The Persian is calm and placid, he likes the peace and sweetness of idleness; he spends most of the day dozing, without being distracted by any movement or noise. The calmness that characterizes him does not make him a "softie"; on the contrary, it often reveals a strong and aristocratic temperament. Even if he does not show it, because he seems distant and on his own, he has a great need of affection which he gives back with fidelity and sympathy. Moreover, conscious of his beauty, he likes to make an effect of it: he wants to be admired and complimented.


## IV - Origin

The Persian breed is artificial because these cats come from a long and careful selection practiced by man from the 19th century. Its ancestor is the Turkish Angora, originating from Turkey. As early as the 17th century, the explorer Pietro della Valle fell in love with this long-coated cat and imported it from Asia Minor to make it known in Europe. In 1880, other Angoras were brought to France and England where the shortest and most round subjects from which the first Persians came were selected; their beauty gave them a huge success: they spread not only in Europe but also in the United States. Today's Persians look different from their ancestors because the selection was refined to obtain hairier and more massive cats.