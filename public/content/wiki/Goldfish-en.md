### Table of content
#### [Food](#i-food)
#### [Tips](#ii-tips)
#### [Adopt](#iii-adopt)
#### [Origin](#iv-origin)


<center>
<img src="/images/poisson-rouge.jpg" alt="drawing" width="300" />
</center>

## I - Food

The goldfish is an omnivorous animal.
In ponds, puddles and mixed ponds (water and plants) not too populated, it finds living and vegetable food in sufficient quantity. In overcrowded ponds, fish tanks and aquariums, they should be fed commercially adapted food as a supplement or as their main meal.

Goldfish are said to be "gluttons" since they can, when fed by humans, eat much more than they need to, at the risk of getting sick. To know how much to give them, you must respect the rule "only give what can be eaten in 3 minutes". It is generally advised to feed goldfish only twice a day minimum. The ration can be adapted according to the fish, their size, their number, their appetite, the temperature, etc.

Preferably choose pellets as they do not sink and do not pollute the water. Avoid dried daphnia, which have no nutritional value, and bread, which swells in their intestines and can cause serious digestive problems.

If their space is not sufficiently planted, it is advisable to complete the menu with greenery (poached salad, spinach, zucchini...) and, if it is not located outside, with live and/or frozen food (artemia, red worms, mosquito larvae...).

## II - Tips

The goldfish is a domestic animal very appreciated for its easy adaptation to the environment and its easy breeding.

It is a gluttonous animal in captivity, whose feeding must be watched. Diseases and deaths occur most often because of poor maintenance (aquarium too small, poor filtration, inadequate food, overcrowding...). In jars or aquariums with too little volume goldfish develop a form of dwarfism which leads to organ malformations, and their life span is considerably shortened.

They can also develop white spots, a sign of a parasitic infection: ichthyophthiriosis (not to be confused with bridal pimples).

## III - Adopt

To summarize, the goldfish is a good first animal because it is not very demanding and easy to raise. However, you should not release your fish in the wild because besides the fact that it is illegal, it can devastate an ecosystem, in small ponds it will be a super predator and will eat all the eggs/young amphibians which are already strongly weakened. In a larger body of water it can hybridize and damage the gene pool of the inhabitants.


## IV - Origin

Goldfish originate from rivers, lakes and ponds in China where their domestication is already mentioned in 970 BC. Before the 16th century, only the nobles raised them. Goldfish were particularly revered during the Song Dynasty (960-1279). They were first kept in rich porcelain jars and then in crystal spheres.