### Sommaire
#### [La nourriture](#i-la-nourriture)
#### [Conseils](#ii-conseils)
#### [Adopter](#iii-adopter)
#### [Origine](#iv-origine)
--------------------------

<center>
<img src="/images/labrador.jpg" alt="drawing" width="300" />
</center>

## I - La Nourriture

C'est un chien très vorace qui a tendance à prendre assez rapidement de l'embonpoint. Attention donc à ne pas lui donner trop à manger ou des aliments trop gras.

## II - Conseils

Le labrador a besoin de se dépenser régulièrement pour ne pas s'empâter, car comme dit précédemment il peut grossir rapidement.

## III - Adopter

Race très docile, joueur et intelligent et d'une allure spectaculaire, le labrador est officiellement approuvé comme chien d'eau et chien de rapport. Cependant, son caractère exceptionnel a fait de lui un chien très recherché comme chien de compagnie et chien de travail, notamment chien guide d'aveugle, ou encore chien détecteur de drogues ou d'explosifs par les forces de l’ordre et des Douanes.


## IV - Origine

L’origine du Labrador a beaucoup de points communs avec celle du Terre-Neuve et il est d’ailleurs assez difficile de distinguer les deux formes originelles. Dans de nombreux textes du XIXe siècle, on utilise sans distinction les termes « Terre-Neuve » et « Labrador » pour évoquer les chiens d’eau des côtes canadiennes. L’ancêtre du Labrador semble être le « Chien de St John », une version plus petite du Terre-Neuve qui s’est développée au Canada à peu près en même temps que ce chien. Le Cão de Castro Laboreiro a probablement également contribué à la formation du Labrador. La race s’est ensuite beaucoup répandue en Grande-Bretagne et c’est ce pays qui en a obtenu la paternité.