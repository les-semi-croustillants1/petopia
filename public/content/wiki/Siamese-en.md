### Table of content
#### [Food](#i-food)
#### [Tips](#ii-tips)
#### [Adopt](#iii-adopt)
#### [Origin](#iv-origin)


<center>
<img src="/images/siamois.jpg" alt="drawing" width="300" />
</center>


## I - Food

The Siamese is a cat of long line type, of average size, with a slender and well muscled body. The cat must be in good physical condition. It is thus necessary to take care not to underfeed him by wanting at all costs to keep him in good health.

## II - Tips

Studies based on data from Swedish insurance companies, have shown that Siamese and its derived breeds have a high mortality rate compared to other breeds. The majority of deaths are due to neoplasms and mammary tumors. Siamese also have a high morbidity rate. The species is more prone to gastrointestinal problems, but can also be compromised by lower urinary tract diseases. It is thus necessary to think of bringing it regularly to the veterinary.

## III - Adopt

He comes as soon as he is called, understands quickly the prohibitions and tends to follow his human companion spontaneously and in many cases systematically. We can easily teach him to walk on a leash and take him everywhere, even on vacation, because, unlike most other cats, he develops an attachment to humans rather than to a territory. Very attached to his master, he can't stand solitude. It is necessary that his master is very present.


## IV - Origin

The origin is in the South-East of Asia: the race could descend from the sacred cats of the temples of Siam (the origin of the name). These cats were already represented in some manuscripts found in Ayutthaya (former capital of Siam) and dating from 1350. A German naturalist also described cats with dark extremities during the nineteenth century. They were also eaten for the quality of their meat.