### Sommaire
#### [La nourriture](#i-la-nourriture)
#### [Conseils](#ii-conseils)
#### [Adopter](#iii-adopter)
#### [Origine](#iv-origine)


<center>
<img src="/images/poisson-rouge.jpg" alt="drawing" width="300" />
</center>

## I - La Nourriture

Le poisson rouge est animal omnivore.
Dans les étangs, les mares et les bassins mixtes (eau et plantes) pas trop peuplés, il trouve en principe de la nourriture vivante et végétale en quantité suffisante. En bassin surpeuplé, en vivier et en aquarium, on lui donnera, en complément ou en menu principal, de la nourriture du commerce adaptée.

Le poisson rouge est dit « glouton » puisqu'il peut, lorsqu'il est nourri par l'homme, manger bien plus qu'il ne lui est nécessaire, au risque d'être malade. Pour savoir quelle quantité leur donner, il faut respecter la règle « ne donner que ce qui peut être mangé en 3 minutes ». On conseille généralement de ne nourrir les poissons rouges que deux fois par jour minimum. On peut adapter ainsi la ration en fonction des poissons, de leur taille, leur nombre, leur appétit, la température, etc.

Choisir de préférence les granulés car ils ne coulent pas et ne polluent pas l'eau. Éviter les daphnies séchées, elles n'ont aucun intérêt nutritif, et le pain, qui gonfle dans leur intestin et peut leur provoquer de graves problèmes de digestion.

Si leur espace n'est pas suffisamment planté, il est conseillé de compléter le menu par de la verdure (salade pochée, épinard, courgette…) et, s'il n'est pas situé à l'extérieur, par de la nourriture vivante et/ou congelée (artémies, vers de vase rouges, larves de moustiques…).

## II - Conseils

Le poisson rouge est un animal domestique très apprécié pour sa facilité d'adaptation à l'environnement et son élevage aisé.

C'est un animal volontiers glouton en captivité, dont il faut surveiller l'alimentation. Les maladies et décès surviennent le plus souvent à cause d'un mauvais entretien (aquarium trop petit, mauvaise filtration, alimentation inadéquate, surpeuplement…). Dans des bocaux ou des aquariums d'un trop faible volume les poissons rouges développent une forme de nanisme qui entraîne des malformations des organes, et leur durée de vie est considérablement raccourcie.

Il peut aussi développer des points blanc, signe d'une infection parasitaire : l'ichtyophthiriose (à ne pas confondre avec des boutons de noces).

## III - Adopter

Pour résumer le poisson rouge est un bon premier animal car peu demandant et facile à élever. Cependant, il ne faut absolument pas relacher ses poissons dans la nature car outre le fait que cela soit illégal, il peut dévaster un écosystème, dans les petites mares il sera un superprédateur et mangera tous les oeufs/jeunes amphibiens qui sont déjà fortement fragilisés. Dans une étendue d'eau plus grande il peut s'hybrider et abimer le patrimoine génétique des habitants.


## IV - Origine

Les poissons rouges sont originaires des rivières, lacs et étangs de Chine où leur domestication est déjà mentionnée en 970 av. J.-C. Avant le XVIe siècle, seuls les nobles les élevaient. Les poissons rouges étaient particulièrement vénérés sous la dynastie Song (960-1279). Ils ont tout d'abord été conservés dans de riches bocaux de porcelaine puis dans des sphères de cristal.