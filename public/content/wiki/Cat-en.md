### Table of content
#### [Food](#i-food)
#### [Tips](#ii-tips)
#### [Adopt](#iii-adopt)
#### [Notes](#iv-notes)
--------------------------

<center>
<img src="/images/chat.jpg" alt="drawing" width="300" />
</center>

## I - Food

The cat is a carnivore. It is therefore quite capable of hunting mice and other small rodents by itself. A particularity of the cat is to eat grass. It is not to feed itself but to force itself to spit out the hair it absorbs during its grooming.

## II - Tips

The cat knows how to groom itself, it is not necessary to wash it. The cat is an animal that owns and protects its territory. It is therefore normal that he fights with other cats from time to time. You should not prevent this, because this would make your cat a submissive animal to others and not possessing a territory, even at home. But of course, you can disinfect the wounds after his fights.

## III - Adopt

An apartment cat doesn't need much space, but it should be able to let off steam on a cat tree. If you have a garden, the cat will have fun outside as he wants. A cat flap at the door to allow him to go outside, is a big plus to save you from getting up when you are on the couch watching a movie and he wants to go outside.


## IV - Notes

The cat is one of the most important pets and has nowdays about fifty different breeds recognized by the certification authorities. In many countries, the cat comes under the legislation on domestic carnivores like the dog and the ferret. Essentially territorial, the cat is a predator of small prey such as rodents or birds. Cats have a variety of vocalizations including purring, meowing, growling or snarling, although they communicate primarily through facial and body postures and pheromones.
