### Sommaire
#### [La nourriture](#i-la-nourriture)
#### [Conseils](#ii-conseils)
#### [Adopter](#iii-adopter)
#### [Notes](#iv-notes)
--------------------------

<center>
<img src="/images/chien.jpg" alt="drawing" width="300" />
</center>

## I - La Nourriture

Le Chien est un carnivore qui peut également adapter son régime alimentaire pour ne manger que des croquettes.

## II - Conseils

Le Chien n'a en général pas besoin de se laver. Si vous le faite ce n'est que pour ne pas salir votre appartemment ou votre maison. 

## III - Adopter

Un chien n’aime pas la solitude. Il a besoin de partager votre quotidien. Un chien est un très bon compagnon pour vos enfants, mais il n’est pas un jouet. Les chiens, en particulier les plus grands, les plus musclés (Terre-Neuve, Boxer, etc.) et les plus vifs (Berger des Pyrénées, terriers, etc.) ont besoin d'espace et d'activité musculaire : jeu, travail, etc. À défaut d'un jardin où l'animal pourrait rester autant de temps qu'il le souhaite, celui-ci a besoin de « sortir » au moins quatre fois par jour (une fois toutes les six heures environ) pendant une vingtaine de minutes environ, pour se « dépenser »,


## IV - Notes

L'étude des chiens et des races de chiens est appelée cynologie. Elle regroupe les approches, les techniques, les philosophies et les divers outils utilisés pour l’éducation canine et le bon comportement des chiens ainsi que leur sélection biologique. Selon les races et les variations génétiques d'un individu à l'autre, les chiens peuvent avoir un pelage très varié. 