-- Do not manually edit this file, it was auto-generated by dillonkearns/elm-graphql
-- https://github.com/dillonkearns/elm-graphql


module PetopiaDB.Object.Booklet_aggregate_fields exposing (..)

import Graphql.Internal.Builder.Argument as Argument exposing (Argument)
import Graphql.Internal.Builder.Object as Object
import Graphql.Internal.Encode as Encode exposing (Value)
import Graphql.Operation exposing (RootMutation, RootQuery, RootSubscription)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet exposing (SelectionSet)
import Json.Decode as Decode
import PetopiaDB.Enum.Booklet_select_column
import PetopiaDB.InputObject
import PetopiaDB.Interface
import PetopiaDB.Object
import PetopiaDB.Scalar
import PetopiaDB.ScalarCodecs
import PetopiaDB.Union


avg :
    SelectionSet decodesTo PetopiaDB.Object.Booklet_avg_fields
    -> SelectionSet (Maybe decodesTo) PetopiaDB.Object.Booklet_aggregate_fields
avg object____ =
    Object.selectionForCompositeField "avg" [] object____ (identity >> Decode.nullable)


type alias CountOptionalArguments =
    { columns : OptionalArgument (List PetopiaDB.Enum.Booklet_select_column.Booklet_select_column)
    , distinct : OptionalArgument Bool
    }


count :
    (CountOptionalArguments -> CountOptionalArguments)
    -> SelectionSet (Maybe Int) PetopiaDB.Object.Booklet_aggregate_fields
count fillInOptionals____ =
    let
        filledInOptionals____ =
            fillInOptionals____ { columns = Absent, distinct = Absent }

        optionalArgs____ =
            [ Argument.optional "columns" filledInOptionals____.columns (Encode.enum PetopiaDB.Enum.Booklet_select_column.toString |> Encode.list), Argument.optional "distinct" filledInOptionals____.distinct Encode.bool ]
                |> List.filterMap identity
    in
    Object.selectionForField "(Maybe Int)" "count" optionalArgs____ (Decode.int |> Decode.nullable)


max :
    SelectionSet decodesTo PetopiaDB.Object.Booklet_max_fields
    -> SelectionSet (Maybe decodesTo) PetopiaDB.Object.Booklet_aggregate_fields
max object____ =
    Object.selectionForCompositeField "max" [] object____ (identity >> Decode.nullable)


min :
    SelectionSet decodesTo PetopiaDB.Object.Booklet_min_fields
    -> SelectionSet (Maybe decodesTo) PetopiaDB.Object.Booklet_aggregate_fields
min object____ =
    Object.selectionForCompositeField "min" [] object____ (identity >> Decode.nullable)


stddev :
    SelectionSet decodesTo PetopiaDB.Object.Booklet_stddev_fields
    -> SelectionSet (Maybe decodesTo) PetopiaDB.Object.Booklet_aggregate_fields
stddev object____ =
    Object.selectionForCompositeField "stddev" [] object____ (identity >> Decode.nullable)


stddev_pop :
    SelectionSet decodesTo PetopiaDB.Object.Booklet_stddev_pop_fields
    -> SelectionSet (Maybe decodesTo) PetopiaDB.Object.Booklet_aggregate_fields
stddev_pop object____ =
    Object.selectionForCompositeField "stddev_pop" [] object____ (identity >> Decode.nullable)


stddev_samp :
    SelectionSet decodesTo PetopiaDB.Object.Booklet_stddev_samp_fields
    -> SelectionSet (Maybe decodesTo) PetopiaDB.Object.Booklet_aggregate_fields
stddev_samp object____ =
    Object.selectionForCompositeField "stddev_samp" [] object____ (identity >> Decode.nullable)


sum :
    SelectionSet decodesTo PetopiaDB.Object.Booklet_sum_fields
    -> SelectionSet (Maybe decodesTo) PetopiaDB.Object.Booklet_aggregate_fields
sum object____ =
    Object.selectionForCompositeField "sum" [] object____ (identity >> Decode.nullable)


var_pop :
    SelectionSet decodesTo PetopiaDB.Object.Booklet_var_pop_fields
    -> SelectionSet (Maybe decodesTo) PetopiaDB.Object.Booklet_aggregate_fields
var_pop object____ =
    Object.selectionForCompositeField "var_pop" [] object____ (identity >> Decode.nullable)


var_samp :
    SelectionSet decodesTo PetopiaDB.Object.Booklet_var_samp_fields
    -> SelectionSet (Maybe decodesTo) PetopiaDB.Object.Booklet_aggregate_fields
var_samp object____ =
    Object.selectionForCompositeField "var_samp" [] object____ (identity >> Decode.nullable)


variance :
    SelectionSet decodesTo PetopiaDB.Object.Booklet_variance_fields
    -> SelectionSet (Maybe decodesTo) PetopiaDB.Object.Booklet_aggregate_fields
variance object____ =
    Object.selectionForCompositeField "variance" [] object____ (identity >> Decode.nullable)
