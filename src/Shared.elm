module Shared exposing
    ( Flags
    , Model
    , Msg
    , init
    , subscriptions
    , update
    , view
    )

import Browser.Navigation as Nav exposing (Key)
import Element exposing (..)
import Element.Font as Font
import Element.Region as Region
import Html.Attributes
import Maybe
import R10.Color
import R10.Color.AttrsBackground
import R10.Color.AttrsFont
import R10.Color.Svg
import R10.Header
import R10.Language
import R10.Libu
import R10.Mode
import R10.Svg.IconsExtra
import R10.Theme
import Spa.Document exposing (Document)
import Spa.Generated.Route as Route exposing (Route)
import Url exposing (Url)
import Utils.Colors
import Utils.Language
import Utils.Translations as Translations
import Utils.Translations.Eula as TEula
import Utils.Translations.HealthRecord as THealthRecord



-- INIT


type alias Flags =
    ()


type alias Model =
    { url : Url
    , key : Key
    , header : R10.Header.Header
    , theme : R10.Theme.Theme
    , route : Route
    , lang : R10.Language.Language
    , token : Maybe String
    }


init : Flags -> Url -> Key -> ( Model, Cmd Msg )
init _ init_url key =
    let
        url =
            { init_url | path = pathFromUrl init_url }

        header =
            R10.Header.init

        lang =
            Utils.Language.fromPath url.path
    in
    ( { url = url
      , key = key
      , header =
            { header
                | backgroundColor = Just <| R10.Color.Svg.primary initTheme
                , supportedLanguageList = Utils.Language.supportedLanguageList
                , urlLogin = ""
                , urlLogout = ""
            }
      , theme = initTheme
      , route = routeFromUrl url
      , lang = Utils.Language.fromLanguagewithDefault lang
      , token = tokenFromUrl url
      }
    , case lang of
        Nothing ->
            if url.path == "/" then
                Nav.replaceUrl key <|
                    Route.toString <|
                        Route.Lang_String__Top { lang = R10.Language.toStringShort R10.Language.default }

            else
                Nav.replaceUrl key <|
                    Route.toString <|
                        Route.Lang_String__NotFound { lang = R10.Language.toStringShort R10.Language.default }

        Just _ ->
            Nav.replaceUrl key <|
                Route.toString <|
                    routeFromUrl url
    )


initTheme : R10.Theme.Theme
initTheme =
    { mode = R10.Mode.default
    , primaryColor = R10.Color.primary.crimsonRed
    }


routeFromUrl : Url -> Route
routeFromUrl =
    Route.fromUrl >> Maybe.withDefault (Route.Lang_String__NotFound { lang = R10.Language.toStringShort R10.Language.default })


tokenFromUrl : Url -> Maybe String
tokenFromUrl url =
    let
        fragment =
            Maybe.withDefault "" url.fragment
    in
    if String.startsWith "access_token" fragment then
        String.split "&" fragment
            |> List.map (String.split "=")
            |> List.foldr
                (\item token ->
                    case item of
                        [ "id_token", id_token ] ->
                            id_token

                        _ ->
                            token
                )
                ""
            |> Just

    else
        Nothing


pathFromUrl : Url -> String
pathFromUrl url =
    let
        fragment =
            Maybe.withDefault "" url.fragment
    in
    if String.startsWith "access_token" fragment then
        String.split "&" fragment
            |> List.map (String.split "=")
            |> List.foldr
                (\item path ->
                    case item of
                        [ "state", state ] ->
                            case Url.fromString <| Maybe.withDefault "" <| Url.percentDecode state of
                                Just stateUrl ->
                                    stateUrl.path

                                Nothing ->
                                    path

                        _ ->
                            path
                )
                "/"

    else
        "/"



-- UPDATE


type Msg
    = HeaderMsg R10.Header.Msg
    | OnClick String
    | ToggleMode


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        HeaderMsg r10msg ->
            ( { model | header = R10.Header.update r10msg model.header }
            , Cmd.none
            )

        OnClick path ->
            let
                header =
                    model.header
            in
            ( { model
                | route =
                    Maybe.withDefault
                        (Route.Lang_String__NotFound
                            { lang = R10.Language.toStringShort R10.Language.default }
                        )
                    <|
                        Route.fromUrl model.url
                , header = R10.Header.closeMenu header
                , lang = Utils.Language.fromLanguagewithDefault <| Utils.Language.fromPath path
              }
            , Cmd.none
            )

        ToggleMode ->
            let
                theme =
                    model.theme

                newTheme =
                    { theme | mode = R10.Mode.toggle theme.mode }
            in
            ( { model | theme = newTheme }
            , Cmd.none
            )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch <| R10.Header.subscriptions model.header HeaderMsg



-- VIEW


view :
    { page : Document msg, toMsg : Msg -> msg }
    -> Model
    -> Document msg
view { page, toMsg } model =
    { title = page.title ++ " - Petopia"
    , body =
        [ column
            [ height fill
            , width fill
            , scrollbars
            ]
            [ viewHeader model toMsg
            , column
                [ height fill
                , width fill
                , Region.mainContent
                , R10.Color.AttrsBackground.background model.theme
                , scrollbars
                ]
                page.body
            ]
        ]
    }


viewHeader : Model -> (Msg -> msg) -> Element msg
viewHeader model toMsg =
    el
        [ width fill
        , R10.Color.AttrsFont.normal model.theme
        ]
    <|
        map toMsg <|
            R10.Header.view model.header (headerFooterArgs model)


headerFooterArgs : Model -> R10.Header.ViewArgs Msg Route
headerFooterArgs model =
    { extraContent = links model
    , extraContentRightSide = rightHeader model
    , msgMapper = HeaderMsg
    , isTop = True
    , from = Route.toString model.route
    , isMobile = False
    , onClick = OnClick
    , urlTop = Route.toString <| Route.Lang_String__Top { lang = R10.Language.toStringShort model.lang }
    , languageSystem =
        R10.Header.LanguageInRoute
            { routeToPath = routeToPath
            , route = model.route
            , routeToLanguage = routeToLanguage
            }
    , logoOnDark =
        el
            [ Font.color <| rgb 1 1 1
            , Font.bold
            , alpha 1
            , mouseOver [ alpha 0.8 ]
            , htmlAttribute <| Html.Attributes.style "transition" "opacity 0.3s"
            ]
        <|
            text <|
                Translations.projectName model.lang
    , logoOnLight =
        el
            [ Font.color <| rgb 0 0 0
            , Font.bold
            , alpha 1
            , mouseOver [ alpha 0.8 ]
            , htmlAttribute <| Html.Attributes.style "transition" "opacity 0.3s"
            ]
        <|
            text <|
                Translations.projectName model.lang
    , darkHeader = True
    , theme = model.theme
    }


links : Model -> List (Element Msg)
links { route, lang } =
    List.map
        (\r ->
            let
                label =
                    text <| routeLabel r
            in
            if route == r then
                el
                    (R10.Header.attrsLink
                        ++ [ Font.bold
                           , htmlAttribute <| Html.Attributes.style "pointer-events" "none"
                           ]
                    )
                    label

            else
                R10.Libu.view
                    R10.Header.attrsLink
                    { label = label
                    , type_ = R10.Libu.LiInternal (Route.toString r) OnClick
                    }
        )
    <|
        routeList lang


rightHeader : Model -> List (Element Msg)
rightHeader model =
    let
        iconAttrs =
            [ alpha 0.8
            , htmlAttribute <| Html.Attributes.style "transition" "opacity 0.3s"
            , mouseOver [ alpha 1 ]
            ]
    in
    [ row
        [ spacing 20
        ]
        ([ R10.Libu.view
            (iconAttrs
                ++ [ htmlAttribute <| Html.Attributes.style "transition" "1s"
                   , Region.description "Toggle Light/Dark Mode"
                   , rotate
                        (if R10.Mode.isLight model.theme.mode then
                            pi

                         else
                            0
                        )
                   ]
            )
            { label = R10.Svg.IconsExtra.darkLight [] Utils.Colors.headerIcon 28
            , type_ = R10.Libu.Bu <| Just ToggleMode
            }
         , R10.Libu.view
            (iconAttrs
                ++ [ Region.description "Petopia in gitlab.com" ]
            )
            { label = image [ width <| px 40 ] { src = "/images/gitlab-icon-1-color-white-rgb.svg", description = "Link to project in gitlab" }
            , type_ = R10.Libu.LiNewTab "https://gitlab.com/les-semi-croustillants1/petopia/"
            }
         ]
            ++ (case model.token of
                    Just _ ->
                        [ R10.Libu.view
                            (iconAttrs
                                ++ [ Region.description "Logout of Petopia!" ]
                            )
                            { label = image [ width <| px 40 ] { src = "/images/logout.svg", description = "Logout" }
                            , type_ =
                                let
                                    url =
                                        model.url

                                    redirect_uri =
                                        { url | path = "", query = Nothing, fragment = Nothing }
                                in
                                R10.Libu.Li <|
                                    "https://petopia.eu.auth0.com"
                                        ++ "/v2/logout"
                                        ++ "?client_id=Cbppu7AeQVKw4oTqP8LKagPydUGNUCbW"
                                        ++ "&returnTo="
                                        ++ Url.toString redirect_uri
                            }
                        ]

                    Nothing ->
                        [ R10.Libu.view
                            (iconAttrs
                                ++ [ Region.description "Login to Petopia!" ]
                            )
                            { label = image [ width <| px 40 ] { src = "/images/login.svg", description = "Login" }
                            , type_ =
                                let
                                    url =
                                        model.url

                                    redirect_uri =
                                        { url | path = "", query = Nothing, fragment = Nothing }
                                in
                                R10.Libu.Li <|
                                    "https://petopia.eu.auth0.com"
                                        ++ "/authorize"
                                        ++ "?client_id=Cbppu7AeQVKw4oTqP8LKagPydUGNUCbW"
                                        ++ "&redirect_uri="
                                        ++ Url.toString redirect_uri
                                        ++ "&scope=openid profile email preferred_username"
                                        ++ "&response_type=token id_token"
                                        ++ "&state="
                                        ++ Url.toString url
                                        ++ "&nonce="
                                        -- could be something random
                                        ++ Url.toString url
                            }
                        ]
               )
        )
    ]


routeToPath : R10.Language.Language -> Route -> String
routeToPath lang route =
    case route of
        Route.Lang_String__Top _ ->
            Route.toString <| Route.Lang_String__Top { lang = R10.Language.toStringShort lang }

        Route.Lang_String__NotFound _ ->
            Route.toString <| Route.Lang_String__NotFound { lang = R10.Language.toStringShort lang }

        Route.Lang_String__Wiki__Top _ ->
            Route.toString <| Route.Lang_String__Wiki__Top { lang = R10.Language.toStringShort lang }

        Route.Lang_String__Wiki__Animal_String__Breed_String { animal, breed } ->
            Route.toString <| Route.Lang_String__Wiki__Animal_String__Breed_String { lang = R10.Language.toStringShort lang, animal = animal, breed = breed }

        Route.Lang_String__Wiki__Animal_String__Top { animal } ->
            Route.toString <| Route.Lang_String__Wiki__Animal_String__Top { lang = R10.Language.toStringShort lang, animal = animal }

        Route.Lang_String__Eula _ ->
            Route.toString <| Route.Lang_String__Eula { lang = R10.Language.toStringShort lang }

        Route.Lang_String__HealthRecord _ ->
            Route.toString <| Route.Lang_String__HealthRecord { lang = R10.Language.toStringShort lang }


routeToLanguage : Route -> R10.Language.Language
routeToLanguage route =
    case route of
        Route.Lang_String__Top { lang } ->
            Utils.Language.fromStringwithDefault lang

        Route.Lang_String__NotFound { lang } ->
            Utils.Language.fromStringwithDefault lang

        Route.Lang_String__HealthRecord { lang } ->
            Utils.Language.fromStringwithDefault lang

        Route.Lang_String__Eula { lang } ->
            Utils.Language.fromStringwithDefault lang

        Route.Lang_String__Wiki__Top { lang } ->
            Utils.Language.fromStringwithDefault lang

        Route.Lang_String__Wiki__Animal_String__Breed_String { lang } ->
            Utils.Language.fromStringwithDefault lang

        Route.Lang_String__Wiki__Animal_String__Top { lang } ->
            Utils.Language.fromStringwithDefault lang


routeList : R10.Language.Language -> List Route
routeList currentLanguage =
    let
        languageParam =
            { lang = R10.Language.toStringShort currentLanguage }
    in
    [ Route.Lang_String__Wiki__Top languageParam
    , Route.Lang_String__HealthRecord languageParam
    , Route.Lang_String__Eula languageParam
    ]


routeLabel : Route -> String
routeLabel route =
    case route of
        Route.Lang_String__Wiki__Top _ ->
            "Wiki"

        Route.Lang_String__Wiki__Animal_String__Top _ ->
            "Wiki"

        Route.Lang_String__Wiki__Animal_String__Breed_String _ ->
            "Wiki"

        Route.Lang_String__HealthRecord { lang } ->
            THealthRecord.healthRecordTitle <| Utils.Language.fromStringwithDefault lang

        Route.Lang_String__Eula { lang } ->
            TEula.eulaTitle <| Utils.Language.fromStringwithDefault lang

        _ ->
            ""
