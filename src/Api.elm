module Api exposing (deleteBooklet, getBooklet, getUserBooklets, insertBooklet, updateBooklet)

import Api.DeleteBooklet as DeleteBooklet
import Api.GetBooklet as GetBooklet
import Api.GetUserBooklets as GetUserBooklets
import Api.InsertBooklet as InsertBooklet
import Api.UpdateBooklet as UpdateBooklet
import Graphql.Http
import Graphql.Operation exposing (RootMutation, RootQuery)
import Graphql.SelectionSet exposing (SelectionSet)
import RemoteData exposing (RemoteData)


getBooklet :
    Int
    -> (RemoteData (Graphql.Http.Error GetBooklet.Data) GetBooklet.Data -> msg)
    -> String
    -> Cmd msg
getBooklet bookletId msg token =
    makeQuery (GetBooklet.bookletByPkSelection bookletId) msg token


getUserBooklets :
    (RemoteData (Graphql.Http.Error GetUserBooklets.Data) GetUserBooklets.Data -> msg)
    -> String
    -> Cmd msg
getUserBooklets msg token =
    makeQuery GetUserBooklets.userBookletsSelection msg token


deleteBooklet :
    Int
    -> (RemoteData (Graphql.Http.Error DeleteBooklet.Data) DeleteBooklet.Data -> msg)
    -> String
    -> Cmd msg
deleteBooklet bookletId msg token =
    makeMutation (DeleteBooklet.deleteBookletByPk bookletId) msg token


insertBooklet :
    InsertBooklet.InsertBookletOneInput
    -> (RemoteData (Graphql.Http.Error InsertBooklet.Data) InsertBooklet.Data -> msg)
    -> String
    -> Cmd msg
insertBooklet insertdata msg token =
    makeMutation (InsertBooklet.insertBookletOne insertdata) msg token


updateBooklet :
    UpdateBooklet.UpdateBookletByPkInput
    -> (RemoteData (Graphql.Http.Error UpdateBooklet.Data) UpdateBooklet.Data -> msg)
    -> String
    -> Cmd msg
updateBooklet insertdata msg token =
    makeMutation (UpdateBooklet.updateBookletByPk insertdata) msg token



-- Graphql General Utils


makeQuery : SelectionSet decodesTo RootQuery -> (RemoteData (Graphql.Http.Error decodesTo) decodesTo -> msg) -> String -> Cmd msg
makeQuery selectionSet msg token =
    selectionSet
        |> Graphql.Http.queryRequest "https://petopia-hasura-staging.herokuapp.com/v1/graphql"
        |> Graphql.Http.withHeader "Authorization" ("Bearer " ++ token)
        |> Graphql.Http.send (RemoteData.fromResult >> msg)


makeMutation : SelectionSet decodesTo RootMutation -> (RemoteData (Graphql.Http.Error decodesTo) decodesTo -> msg) -> String -> Cmd msg
makeMutation selectionSet msg token =
    selectionSet
        |> Graphql.Http.mutationRequest "https://petopia-hasura-staging.herokuapp.com/v1/graphql"
        |> Graphql.Http.withHeader "Authorization" ("Bearer " ++ token)
        |> Graphql.Http.send (RemoteData.fromResult >> msg)
