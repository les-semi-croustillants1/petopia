module Api.GetBooklet exposing (Data, bookletByPkSelection)

import Api.Data exposing (..)
import Graphql.Operation exposing (RootQuery)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet, with)
import PetopiaDB.Object
import PetopiaDB.Object.Animal
import PetopiaDB.Object.Booklet
import PetopiaDB.Object.User
import PetopiaDB.Object.Vaccine
import PetopiaDB.Object.Veterinary
import PetopiaDB.Query as Query


type alias Data =
    { booklet : Maybe Booklet }


bookletByPkSelection : Int -> SelectionSet Data RootQuery
bookletByPkSelection bookletId =
    SelectionSet.succeed Data
        |> with
            (Query.booklet_by_pk
                (Query.BookletByPkRequiredArguments bookletId)
                bookletSelection
            )


bookletSelection : SelectionSet Booklet PetopiaDB.Object.Booklet
bookletSelection =
    SelectionSet.succeed
        Booklet
        |> with PetopiaDB.Object.Booklet.birth_date
        |> with PetopiaDB.Object.Booklet.breed
        |> with PetopiaDB.Object.Booklet.color
        |> with PetopiaDB.Object.Booklet.diseases
        |> with PetopiaDB.Object.Booklet.electronic_id
        |> with PetopiaDB.Object.Booklet.name
        |> with PetopiaDB.Object.Booklet.notes
        |> with PetopiaDB.Object.Booklet.sex
        |> with PetopiaDB.Object.Booklet.tatoo_number
        |> with PetopiaDB.Object.Booklet.treatment
        |> with (PetopiaDB.Object.Booklet.animal animalSelection)


animalSelection : SelectionSet Animal PetopiaDB.Object.Animal
animalSelection =
    SelectionSet.succeed
        Animal
        |> with (PetopiaDB.Object.Animal.vaccines (\optionals -> optionals) vaccineSelection)


vaccineSelection : SelectionSet Vaccine PetopiaDB.Object.Vaccine
vaccineSelection =
    SelectionSet.succeed
        Vaccine
        |> with PetopiaDB.Object.Vaccine.date
        |> with PetopiaDB.Object.Vaccine.injection_spot
        |> with PetopiaDB.Object.Vaccine.name
        |> with PetopiaDB.Object.Vaccine.serial_num
        |> with (PetopiaDB.Object.Vaccine.veterinary veterinarySelection)


veterinarySelection : SelectionSet Veterinary PetopiaDB.Object.Veterinary
veterinarySelection =
    SelectionSet.succeed
        Veterinary
        |> with PetopiaDB.Object.Veterinary.phone_number
        |> with PetopiaDB.Object.Veterinary.work_adress
        |> with PetopiaDB.Object.Veterinary.zipcode
        |> with PetopiaDB.Object.Veterinary.username
