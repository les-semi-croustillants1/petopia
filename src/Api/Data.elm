module Api.Data exposing (Animal, Booklet, Vaccine, Veterinary)

import PetopiaDB.Scalar


type alias Booklet =
    { birth_date : PetopiaDB.Scalar.Date
    , breed : Maybe String
    , color : String
    , diseases : Maybe String
    , electronic_id : Maybe String
    , name : String
    , notes : Maybe String
    , sex : String
    , tatoo_number : Maybe String
    , treatment : Maybe String
    , animal : Animal
    }


type alias Animal =
    { vaccines : List Vaccine }


type alias Vaccine =
    { date : PetopiaDB.Scalar.Date
    , injection_spot : Maybe String
    , name : String
    , serial_num : Maybe String
    , veterinary : Veterinary
    }


type alias Veterinary =
    { phone_number : String
    , work_adress : String
    , zipcode : String
    , username : String
    }
