module Api.UpdateBooklet exposing (Data, UpdateBookletByPkInput, updateBookletByPk)

import Api.Data exposing (..)
import Graphql.Operation exposing (RootMutation)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet, with)
import PetopiaDB.InputObject
import PetopiaDB.Mutation as Mutation
import PetopiaDB.Object
import PetopiaDB.Object.Animal
import PetopiaDB.Object.Booklet
import PetopiaDB.Object.User
import PetopiaDB.Object.Vaccine
import PetopiaDB.Object.Veterinary
import PetopiaDB.Scalar


type alias Data =
    { booklet : Maybe Booklet }


type alias UpdateBookletByPkInput =
    { booklet_id : Int
    , name : Maybe String
    , birth_date : Maybe PetopiaDB.Scalar.Date
    , sex : Maybe String
    , color : Maybe String
    , breed : Maybe String
    , tatoo_number : Maybe String
    , electronic_id : Maybe String
    , diseases : Maybe String
    , treatment : Maybe String
    , notes : Maybe String
    }


updateBookletByPk : UpdateBookletByPkInput -> SelectionSet Data RootMutation
updateBookletByPk { booklet_id, name, birth_date, sex, color, breed, tatoo_number, electronic_id, diseases, treatment, notes } =
    SelectionSet.succeed
        Data
        |> with
            (Mutation.update_booklet_by_pk
                (\optionalArgs ->
                    { optionalArgs
                        | set_ =
                            Present <|
                                PetopiaDB.InputObject.buildBooklet_set_input
                                    (\set_ ->
                                        { set_
                                            | name = Graphql.OptionalArgument.fromMaybe name
                                            , birth_date = Graphql.OptionalArgument.fromMaybe birth_date
                                            , sex = Graphql.OptionalArgument.fromMaybe sex
                                            , color = Graphql.OptionalArgument.fromMaybe color
                                            , breed = Graphql.OptionalArgument.fromMaybe breed
                                            , tatoo_number = Graphql.OptionalArgument.fromMaybe tatoo_number
                                            , electronic_id = Graphql.OptionalArgument.fromMaybe electronic_id
                                            , diseases = Graphql.OptionalArgument.fromMaybe diseases
                                            , treatment = Graphql.OptionalArgument.fromMaybe treatment
                                            , notes = Graphql.OptionalArgument.fromMaybe notes
                                        }
                                    )
                    }
                )
                (Mutation.UpdateBookletByPkRequiredArguments <| PetopiaDB.InputObject.buildBooklet_pk_columns_input <| PetopiaDB.InputObject.Booklet_pk_columns_inputRequiredFields booklet_id)
                bookletSelection
            )


bookletSelection : SelectionSet Booklet PetopiaDB.Object.Booklet
bookletSelection =
    SelectionSet.succeed
        Booklet
        |> with PetopiaDB.Object.Booklet.birth_date
        |> with PetopiaDB.Object.Booklet.breed
        |> with PetopiaDB.Object.Booklet.color
        |> with PetopiaDB.Object.Booklet.diseases
        |> with PetopiaDB.Object.Booklet.electronic_id
        |> with PetopiaDB.Object.Booklet.name
        |> with PetopiaDB.Object.Booklet.notes
        |> with PetopiaDB.Object.Booklet.sex
        |> with PetopiaDB.Object.Booklet.tatoo_number
        |> with PetopiaDB.Object.Booklet.treatment
        |> with (PetopiaDB.Object.Booklet.animal animalSelection)


animalSelection : SelectionSet Animal PetopiaDB.Object.Animal
animalSelection =
    SelectionSet.succeed
        Animal
        |> with (PetopiaDB.Object.Animal.vaccines (\optionals -> optionals) vaccineSelection)


vaccineSelection : SelectionSet Vaccine PetopiaDB.Object.Vaccine
vaccineSelection =
    SelectionSet.succeed
        Vaccine
        |> with PetopiaDB.Object.Vaccine.date
        |> with PetopiaDB.Object.Vaccine.injection_spot
        |> with PetopiaDB.Object.Vaccine.name
        |> with PetopiaDB.Object.Vaccine.serial_num
        |> with (PetopiaDB.Object.Vaccine.veterinary veterinarySelection)


veterinarySelection : SelectionSet Veterinary PetopiaDB.Object.Veterinary
veterinarySelection =
    SelectionSet.succeed
        Veterinary
        |> with PetopiaDB.Object.Veterinary.phone_number
        |> with PetopiaDB.Object.Veterinary.work_adress
        |> with PetopiaDB.Object.Veterinary.zipcode
        |> with PetopiaDB.Object.Veterinary.username
