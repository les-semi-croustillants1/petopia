module Api.DeleteBooklet exposing (Booklet, Data, deleteBookletByPk)

import Graphql.Operation exposing (RootMutation)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet, with)
import PetopiaDB.Mutation as Mutation
import PetopiaDB.Object
import PetopiaDB.Object.Booklet



{-
   {
     "data": {
       "delete_booklet_by_pk": null
     }
   }
-}


type alias Data =
    { booklet : Maybe Booklet }


type alias Booklet =
    { id : Int }


deleteBookletByPk : Int -> SelectionSet Data RootMutation
deleteBookletByPk bookletId =
    SelectionSet.succeed Data
        |> with
            (Mutation.delete_booklet_by_pk
                (Mutation.DeleteBookletByPkRequiredArguments bookletId)
                bookletSelection
            )


bookletSelection : SelectionSet Booklet PetopiaDB.Object.Booklet
bookletSelection =
    SelectionSet.succeed
        Booklet
        |> with PetopiaDB.Object.Booklet.id
