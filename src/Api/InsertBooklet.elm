module Api.InsertBooklet exposing (Data, InsertBookletOneInput, insertBookletOne)

import Graphql.Operation exposing (RootMutation)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet, with)
import PetopiaDB.InputObject
import PetopiaDB.Mutation as Mutation
import PetopiaDB.Object
import PetopiaDB.Object.Booklet
import PetopiaDB.Scalar


type alias Data =
    { booklet : Maybe Booklet }


type alias Booklet =
    { id : Int }


type alias InsertBookletOneInput =
    { name : String
    , birth_date : PetopiaDB.Scalar.Date
    , sex : String
    , color : String
    , breed : Maybe String
    , tatoo_number : Maybe String
    , electronic_id : Maybe String
    , diseases : Maybe String
    , treatment : Maybe String
    , notes : Maybe String
    }


insertBookletOne : InsertBookletOneInput -> SelectionSet Data RootMutation
insertBookletOne { name, birth_date, sex, color, breed, tatoo_number, electronic_id, diseases, treatment, notes } =
    SelectionSet.succeed
        Data
        |> with
            (Mutation.insert_booklet_one
                (\optionalArgs -> optionalArgs)
                (Mutation.InsertBookletOneRequiredArguments
                    (PetopiaDB.InputObject.buildBooklet_insert_input
                        (\object ->
                            { object
                                | animal =
                                    Present <|
                                        PetopiaDB.InputObject.buildAnimal_obj_rel_insert_input
                                            (PetopiaDB.InputObject.Animal_obj_rel_insert_inputRequiredFields
                                                (PetopiaDB.InputObject.buildAnimal_insert_input
                                                    (\animal ->
                                                        { animal
                                                            | owner_animals =
                                                                Present <|
                                                                    PetopiaDB.InputObject.buildOwner_animal_arr_rel_insert_input
                                                                        (PetopiaDB.InputObject.Owner_animal_arr_rel_insert_inputRequiredFields
                                                                            [ PetopiaDB.InputObject.buildOwner_animal_insert_input
                                                                                (\owner_animal -> owner_animal)
                                                                            ]
                                                                        )
                                                                        (\optionalArgs -> optionalArgs)
                                                        }
                                                    )
                                                )
                                            )
                                            (\optionalArgs -> optionalArgs)
                                , name = Present name
                                , birth_date = Present birth_date
                                , sex = Present sex
                                , color = Present color
                                , breed = Graphql.OptionalArgument.fromMaybe breed
                                , tatoo_number = Graphql.OptionalArgument.fromMaybe tatoo_number
                                , electronic_id = Graphql.OptionalArgument.fromMaybe electronic_id
                                , diseases = Graphql.OptionalArgument.fromMaybe diseases
                                , treatment = Graphql.OptionalArgument.fromMaybe treatment
                                , notes = Graphql.OptionalArgument.fromMaybe notes
                            }
                        )
                    )
                )
                bookletSelection
            )


bookletSelection : SelectionSet Booklet PetopiaDB.Object.Booklet
bookletSelection =
    SelectionSet.succeed
        Booklet
        |> with PetopiaDB.Object.Booklet.id
