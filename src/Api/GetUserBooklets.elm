module Api.GetUserBooklets exposing (Booklet, Data, userBookletsSelection)

import Graphql.Operation exposing (RootQuery)
import Graphql.OptionalArgument exposing (OptionalArgument(..))
import Graphql.SelectionSet as SelectionSet exposing (SelectionSet, with)
import PetopiaDB.InputObject
import PetopiaDB.Object
import PetopiaDB.Object.Booklet
import PetopiaDB.Query as Query



{- query MyQuery2 {
     booklet(where: {animal: {owner_animals: {owner_id: {_eq: 1}}}}) {
       id
       name
     }
   }
-}


type alias Data =
    { booklets : List Booklet }


type alias Booklet =
    { id : Int
    , name : String
    }


userBookletsSelection : SelectionSet Data RootQuery
userBookletsSelection =
    SelectionSet.succeed Data
        |> with
            (Query.booklet
                (\optionals -> optionals)
                bookletSelection
            )


bookletSelection : SelectionSet Booklet PetopiaDB.Object.Booklet
bookletSelection =
    SelectionSet.succeed
        Booklet
        |> with PetopiaDB.Object.Booklet.id
        |> with PetopiaDB.Object.Booklet.name
