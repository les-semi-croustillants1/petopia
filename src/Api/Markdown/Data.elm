module Api.Markdown.Data exposing (Data(..), fromHttpResult, view)

import Html exposing (Html)
import Html.Attributes as Attr


type Data value
    = Loading
    | Success value
    | Failure String


fromHttpResult : Result error value -> Data value
fromHttpResult result =
    case result of
        Ok value ->
            Success value

        Err _ ->
            Failure "Either this is a broken link or there's missing documentation!"


view : (value -> Html msg) -> Data value -> Html msg
view toHtml data =
    Html.div
        [ Attr.classList [ ( "invisible", data == Loading ) ]
        ]
    <|
        case data of
            Loading ->
                []

            Success value ->
                [ toHtml value ]

            Failure reason ->
                [ Html.text "Problem in the wiki (api/data.elm) \n   Error : \n"
                , Html.text reason
                ]
