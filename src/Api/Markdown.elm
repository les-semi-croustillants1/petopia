module Api.Markdown exposing (get)

import Api.Markdown.Data exposing (Data)
import Http


get : { file : String, onResponse : Data String -> msg } -> Cmd msg
get options =
    Http.get
        { url = "/content/" ++ options.file
        , expect = Http.expectString (Api.Markdown.Data.fromHttpResult >> options.onResponse)
        }
