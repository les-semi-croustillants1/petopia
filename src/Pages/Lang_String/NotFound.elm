module Pages.Lang_String.NotFound exposing (Model, Msg, Params, page)

import Element exposing (..)
import Element.Font as Font
import R10.Color.AttrsFont
import R10.Form exposing (msg)
import R10.Language
import R10.Theme
import Shared
import Spa.Document exposing (Document)
import Spa.Page as Page exposing (Page)
import Spa.Url exposing (Url)
import Utils.Language
import Utils.Translations.NotFound as TNotFound


page : Page Params Model Msg
page =
    Page.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , save = save
        , load = load
        }



-- INIT


type alias Params =
    { lang : String }


type alias Model =
    { lang : R10.Language.Language
    , theme : R10.Theme.Theme
    }


init : Shared.Model -> Url Params -> ( Model, Cmd Msg )
init shared { params } =
    ( { lang = Utils.Language.fromStringwithDefault params.lang
      , theme = shared.theme
      }
    , Cmd.none
    )



-- UPDATE


type Msg
    = ReplaceMe


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ReplaceMe ->
            ( model, Cmd.none )


save : Model -> Shared.Model -> Shared.Model
save _ shared =
    shared


load : Shared.Model -> Model -> ( Model, Cmd Msg )
load shared model =
    ( { model | theme = shared.theme }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Document Msg
view model =
    { title = TNotFound.notFoundTitle model.lang
    , body =
        [ row
            [ width fill
            , height fill
            ]
            [ viewNotFound model ]
        ]
    }


viewNotFound : Model -> Element msg
viewNotFound model =
    paragraph
        [ Font.center
        , centerY
        , Font.size 40
        , R10.Color.AttrsFont.normal model.theme
        ]
        [ text <|
            TNotFound.notFoundContent model.lang
        ]
