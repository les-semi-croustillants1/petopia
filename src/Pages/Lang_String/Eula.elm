module Pages.Lang_String.Eula exposing (Model, Msg, Params, page)

import Element exposing (..)
import Element.Font as Font
import R10.Color.AttrsFont
import R10.Language
import R10.Theme
import Shared
import Spa.Document exposing (Document)
import Spa.Page as Page exposing (Page)
import Spa.Url as Url exposing (Url)
import Utils.Language
import Utils.Translations.Eula as TEula


page : Page Params Model Msg
page =
    Page.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , save = save
        , load = load
        }



-- INIT


type alias Params =
    { lang : String }


type alias Model =
    { theme : R10.Theme.Theme
    , lang : R10.Language.Language
    }


init : Shared.Model -> Url Params -> ( Model, Cmd Msg )
init shared { params } =
    ( { theme = shared.theme
      , lang = Utils.Language.fromStringwithDefault params.lang
      }
    , Cmd.none
    )



-- UPDATE


type Msg
    = None


update : Msg -> Model -> ( Model, Cmd Msg )
update _ model =
    ( model, Cmd.none )


save : Model -> Shared.Model -> Shared.Model
save _ shared =
    shared


load : Shared.Model -> Model -> ( Model, Cmd Msg )
load shared model =
    ( { model | theme = shared.theme }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Document Msg
view model =
    { title = TEula.eulaTitle model.lang
    , body =
        [ column
            [ paddingXY 200 100
            , spacing 50
            , Font.justify
            , R10.Color.AttrsFont.normal model.theme
            ]
            [ paragraph []
                [ column
                    [ spacing 20
                    ]
                    [ el [ centerX, Font.bold ] (text "Conditions générales d'utilisation")
                    , el [ centerX ] (text "En vigueur au 3/4/2021")
                    , el [] (text "Les présentes conditions générales d'utilisation (dites « CGU ») ont pour objet l'encadrement juridique des modalités de mise à disposition du site et des services par 'EQUIPE 96-Petopia' et de définir les conditions d’accès et d’utilisation des services par « l'Utilisateur ».Les présentes CGU sont accessibles sur le site à la rubrique «CGU». Toute inscription ou utilisation du site implique l'acceptation sans aucune réserve ni restriction des présentes CGU par l’utilisateur. Lors de l'inscription sur le site via le Formulaire d’inscription, chaque utilisateur accepte expressément les présentes CGU en cochant la case précédant le texte suivant : « Je reconnais avoir lu et compris les CGU et je les accepte ».En cas de non-acceptation des CGU stipulées dans le présent contrat, l'Utilisateur se doit de renoncer à l'accès des services proposés par le site. 'EQUIPE 96-Petopia' se réserve le droit de modifier unilatéralement et à tout moment le contenu des présentes CGU.")
                    ]
                ]
            , paragraph []
                [ column
                    [ spacing 10
                    ]
                    [ el [ centerX, Font.bold, padding 20 ] (text "Article 1 : Les mentions légales")
                    , el [] (text "L'édition du site https://petopia-app.netlify.app/en est assurée par la Société SARL petopia au capital de 0 euros, immatriculée au RCS de PARIS sous le numéro (pas encore immatriculé), dont le siège social est situé à EFREI - Villejuif")
                    , el [] (text "Numéro de téléphone +33 755677363")
                    , el [] (text "Adresse e-mail : petopia@gmail.com.")
                    , el [] (text "Le Directeur de la publication est : Equipe 96")
                    , el [] (text "L'hébergeur du site https://petopia-app.netlify.app/en est la société netlify, dont le siège social est situé au Dogpatch, San Francisco, CA, USA, avec le numéro de téléphone : press@netlify.com.")
                    ]
                ]
            , paragraph []
                [ column
                    [ spacing 10
                    ]
                    [ el [ centerX, Font.bold, padding 20 ] (text "ARTICLE 2 : Accès au site")
                    , el [] (text "Le site https://petopia-app.netlify.app/en  permet à l'Utilisateur un accès gratuit aux services suivants :")
                    , el [] (text "Le site internet propose les services suivants :")
                    , el [] (text "")
                    , el [] (text "Allow people to:")
                    , el [] (text "Easily access their pets health book. Find petsitters nearby. Find veterinarians nearby. Find kennels nearby. Find Products for their pets. Access knowledge about pets")
                    , el [] (text "")
                    , el [] (text "Allow veterinaries, petsitters and kennels to:")
                    , el [] (text "Share their informations to be findable")
                    , el [] (text "")
                    , el [] (text "Allow companies to:")
                    , el [] (text "Promote their products if it respect our conditions")
                    , el [] (text "Have their products graded fairly ")
                    , el [] (text "")
                    , el [] (text "Le site est accessible gratuitement en tout lieu à tout Utilisateur ayant un accès à Internet. Tous les frais supportés par l'Utilisateur pour accéder au service (matériel informatique, logiciels, connexion Internet, etc.) sont à sa charge.")
                    , el [] (text "L’Utilisateur non membre n'a pas accès aux services réservés. Pour cela, il doit s’inscrire en remplissant le formulaire. En acceptant de s’inscrire aux services réservés, l’Utilisateur membre s’engage à fournir des informations sincères et exactes concernant son état civil et ses coordonnées, notamment son adresse email.")
                    , el [] (text "Pour accéder aux services, l’Utilisateur doit ensuite s'identifier à l'aide de son identifiant et de son mot de passe qui lui seront communiqués après son inscription.")
                    , el [] (text "Tout Utilisateur membre régulièrement inscrit pourra également solliciter sa désinscription en se rendant à la page dédiée sur son espace personnel. Celle-ci sera effective dans un délai raisonnable. Tout événement dû à un cas de force majeure ayant pour conséquence un dysfonctionnement du site ou serveur et sous réserve de toute interruption ou modification en cas de maintenance, n'engage pas la responsabilité de https://petopia-app.netlify.app/en. Dans ces cas, l’Utilisateur accepte ainsi ne pas tenir rigueur à l’éditeur de toute interruption ou suspension de service, même sans préavis.")
                    , el [] (text "L'Utilisateur a la possibilité de contacter le site par messagerie électronique à l’adresse email de l’éditeur communiqué à l’ARTICLE 1.")
                    ]
                ]
            , paragraph []
                [ column
                    [ spacing 10
                    ]
                    [ el [ centerX, Font.bold, padding 20 ] (text "ARTICLE 3 : Collecte des données")
                    , el [] (text "Le site assure à l'Utilisateur une collecte et un traitement d'informations personnelles dans le respect de la vie privée conformément à la loi n°78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés. Le site est déclaré à la CNIL sous le numéro (non déclaré à ce jour).")
                    , el [] (text "En vertu de la loi Informatique et Libertés, en date du 6 janvier 1978, l'Utilisateur dispose d'un droit d'accès, de rectification, de suppression et d'opposition de ses données personnelles. L'Utilisateur exerce ce droit :")
                    , el [] (text "par mail à l'adresse email petopia@gmail.com")
                    , el [] (text "via un formulaire de contact")
                    , el [] (text "ia son espace personnel")
                    ]
                ]
            , paragraph []
                [ column
                    [ spacing 10
                    ]
                    [ el [ centerX, Font.bold, padding 20 ] (text "ARTICLE 4 : Propriété intellectuelle")
                    , el [] (text "Les marques, logos, signes ainsi que tous les contenus du site (textes, images, son…) font l'objet d'une protection par le Code de la propriété intellectuelle et plus particulièrement par le droit d'auteur.\n\nL'Utilisateur doit solliciter l'autorisation préalable du site pour toute reproduction, publication, copie des différents contenus. Il s'engage à une utilisation des contenus du site dans un cadre strictement privé, toute utilisation à des fins commerciales et publicitaires est strictement interdite.\n")
                    , el [] (text "Toute représentation totale ou partielle de ce site par quelque procédé que ce soit, sans l’autorisation expresse de l’exploitant du site Internet constituerait une contrefaçon sanctionnée par l’article L 335-2 et suivants du Code de la propriété intellectuelle.")
                    , el [] (text "Il est rappelé conformément à l’article L122-5 du Code de propriété intellectuelle que l’Utilisateur qui reproduit, copie ou publie le contenu protégé doit citer l’auteur et sa source.")
                    ]
                ]
            , paragraph []
                [ column
                    [ spacing 10
                    ]
                    [ el [ centerX, Font.bold, padding 20 ] (text "ARTICLE 5 : Responsabilité")
                    , el [] (text "Les sources des informations diffusées sur le site https://petopia-app.netlify.app/en sont réputées fiables mais le site ne garantit pas qu’il soit exempt de défauts, d’erreurs ou d’omissions.")
                    , el [] (text "Les informations communiquées sont présentées à titre indicatif et général sans valeur contractuelle. Malgré des mises à jour régulières, le site https://petopia-app.netlify.app/en ne peut être tenu responsable de la modification des dispositions administratives et juridiques survenant après la publication. De même, le site ne peut être tenue responsable de l’utilisation et de l’interprétation de l’information contenue dans ce site.")
                    , el [] (text "L'Utilisateur s'assure de garder son mot de passe secret. Toute divulgation du mot de passe, quelle que soit sa forme, est interdite. Il assume les risques liés à l'utilisation de son identifiant et mot de passe. Le site décline toute responsabilité.")
                    , el [] (text "Le site https://petopia-app.netlify.app/en ne peut être tenu pour responsable d’éventuels virus qui pourraient infecter l’ordinateur ou tout matériel informatique de l’Internaute, suite à une utilisation, à l’accès, ou au téléchargement provenant de ce site.")
                    , el [] (text "La responsabilité du site ne peut être engagée en cas de force majeure ou du fait imprévisible et insurmontable d'un tiers.")
                    ]
                ]
            , paragraph []
                [ column
                    [ spacing 10
                    ]
                    [ el [ centerX, Font.bold, padding 20 ] (text "ARTICLE 6 : Liens hypertextes")
                    , el [] (text "Des liens hypertextes peuvent être présents sur le site. L’Utilisateur est informé qu’en cliquant sur ces liens, il sortira du site https://petopia-app.netlify.app/en. Ce dernier n’a pas de contrôle sur les pages web sur lesquelles aboutissent ces liens et ne saurait, en aucun cas, être responsable de leur contenu.")
                    ]
                ]
            , paragraph []
                [ column
                    [ spacing 10
                    ]
                    [ el [ centerX, Font.bold, padding 20 ] (text "ARTICLE 7 : Cookies")
                    , el [] (text "L’Utilisateur est informé que lors de ses visites sur le site, un cookie peut s’installer automatiquement sur son logiciel de navigation.")
                    , el [] (text "Les cookies sont de petits fichiers stockés temporairement sur le disque dur de l’ordinateur de l’Utilisateur par votre navigateur et qui sont nécessaires à l’utilisation du site https://petopia-app.netlify.app/en")
                    , el [] (text "Les cookies ne contiennent pas d’information personnelle et ne peuvent pas être utilisés pour identifier quelqu’un. Un cookie contient un identifiant unique, généré aléatoirement et donc anonyme. Certains cookies expirent à la fin de la visite de l’Utilisateur, d’autres restent.")
                    , el [] (text "L’information contenue dans les cookies est utilisée pour améliorer le site https://petopia-app.netlify.app/en. En naviguant sur le site, L’Utilisateur les accepte.")
                    , el [] (text "L’Utilisateur doit toutefois donner son consentement quant à l’utilisation de certains cookies. A défaut d’acceptation, l’Utilisateur est informé que certaines fonctionnalités ou pages risquent de lui être refusées.")
                    , el [] (text "L’Utilisateur pourra désactiver ces cookies par l’intermédiaire des paramètres figurant au sein de son logiciel de navigation.")
                    ]
                ]
            , paragraph []
                [ column
                    [ spacing 10
                    ]
                    [ el [ centerX, Font.bold, padding 20 ] (text "ARTICLE 8 : Publication par l’Utilisateur")
                    , el [] (text "Le site permet aux membres de publier les contenus suivants :")
                    , el [] (text "Posts sur les animaux.")
                    , el [] (text "")
                    , el [] (text "Dans ses publications, le membre s’engage à respecter les règles de la Netiquette (règles de bonne conduite de l’internet) et les règles de droit en vigueur.")
                    , el [] (text "Le site peut exercer une modération sur les publications et se réserve le droit de refuser leur mise en ligne, sans avoir à s’en justifier auprès du membre.")
                    , el [] (text "Le membre reste titulaire de l’intégralité de ses droits de propriété intellectuelle. Mais en publiant une publication sur le site, il cède à la société éditrice le droit non exclusif et gratuit de représenter, reproduire, adapter, modifier, diffuser et distribuer sa publication, directement ou par un tiers autorisé, dans le monde entier, sur tout support (numérique ou physique), pour la durée de la propriété intellectuelle. Le Membre cède notamment le droit d'utiliser sa publication sur internet et sur les réseaux de téléphonie mobile.")
                    , el [] (text "La société éditrice s'engage à faire figurer le nom du membre à proximité de chaque utilisation de sa publication.")
                    , el [] (text "Tout contenu mis en ligne par l'Utilisateur est de sa seule responsabilité. L'Utilisateur s'engage à ne pas mettre en ligne de contenus pouvant porter atteinte aux intérêts de tierces personnes. Tout recours en justice engagé par un tiers lésé contre le site sera pris en charge par l'Utilisateur.")
                    , el [] (text "Le contenu de l'Utilisateur peut être à tout moment et pour n'importe quelle raison supprimé ou modifié par le site, sans préavis.")
                    ]
                ]
            , paragraph []
                [ column
                    [ spacing 10
                    ]
                    [ el [ centerX, Font.bold, padding 20 ] (text "ARTICLE 9 : Droit applicable et juridiction compétente")
                    , el [] (text "La législation française s'applique au présent contrat. En cas d'absence de résolution amiable d'un litige né entre les parties, les tribunaux français seront seuls compétents pour en connaître. Pour toute question relative à l’application des présentes CGU, vous pouvez joindre l’éditeur aux coordonnées inscrites à l’ARTICLE 1.")
                    , el [] (text "CGU réalisées sur http://legalplace.fr/")
                    ]
                ]
            ]
        ]
    }
