module Pages.Lang_String.Top exposing (Model, Msg, Params, page)

import Browser.Navigation as Nav
import Element exposing (..)
import Element.Font as Font
import Embed.Youtube
import Embed.Youtube.Attributes
import R10.Color.AttrsBackground
import R10.Color.AttrsFont
import R10.Language
import R10.Theme
import Shared
import Spa.Document exposing (Document)
import Spa.Generated.Route as Route
import Spa.Page as Page exposing (Page)
import Spa.Url exposing (Url)
import Utils.Language
import Utils.Translations as Translations
import Utils.Translations.Home as THome


page : Page Params Model Msg
page =
    Page.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , save = save
        , load = load
        }



-- INIT


type alias Params =
    { lang : String }


type alias Model =
    { lang : R10.Language.Language
    , theme : R10.Theme.Theme
    }


init : Shared.Model -> Url Params -> ( Model, Cmd Msg )
init shared { params } =
    case Utils.Language.fromString params.lang of
        Just lang ->
            ( { lang = lang
              , theme = shared.theme
              }
            , Cmd.none
            )

        Nothing ->
            ( { lang = R10.Language.default
              , theme = shared.theme
              }
            , Nav.pushUrl shared.key <|
                Route.toString <|
                    Route.Lang_String__NotFound { lang = R10.Language.toStringShort R10.Language.EN_US }
            )



-- UPDATE


type Msg
    = ReplaceMe


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        ReplaceMe ->
            ( model, Cmd.none )


save : Model -> Shared.Model -> Shared.Model
save _ shared =
    shared


load : Shared.Model -> Model -> ( Model, Cmd Msg )
load shared model =
    ( { model | theme = shared.theme }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Document Msg
view model =
    { title = THome.homepageTitle model.lang
    , body =
        [ column
            [ width fill
            , height fill
            , R10.Color.AttrsBackground.background model.theme
            ]
            [ firstSection model
            , secondSection model
            , thirdSection model
            , fourthSection model
            ]
        ]
    }


firstSection : Model -> Element Msg
firstSection model =
    row [ width fill ]
        [ column
            [ width fill
            , spacing 10
            ]
            [ paragraph
                [ Font.center
                , Font.size 50
                , R10.Color.AttrsFont.normal model.theme
                ]
                [ column
                    [ spacing 10 ]
                    [ el [ centerX, Font.bold ] <| text <| Translations.projectName model.lang
                    , el [ centerX ] <| text <| THome.catchPhrase model.lang
                    ]
                ]
            , el
                [ centerX ]
              <|
                html
                    (Embed.Youtube.fromString "FxGlxwCqVeI"
                        |> Embed.Youtube.attributes
                            [ Embed.Youtube.Attributes.width 700
                            , Embed.Youtube.Attributes.height 400
                            ]
                        |> Embed.Youtube.toHtml
                    )
            ]
        , image
            [ width fill ]
            { src = "images/chien-blanc-homepage.jpg"
            , description = "chien"
            }
        ]


secondSection : Model -> Element Msg
secondSection model =
    row [ width fill ]
        [ image
            [ width fill ]
            { src = "images/chien-colrette-homepage.jpg"
            , description = "chien"
            }
        , paragraph
            [ Font.center
            , R10.Color.AttrsFont.normal model.theme
            , width fill
            ]
            [ column
                [ spacing 10 ]
                [ el [ centerX, Font.size 50 ] <| text (THome.secondSectionTitle model.lang)
                , el [ centerX, Font.size 40, paddingXY 25 0 ] <| text (THome.secondSectionBody model.lang)
                ]
            ]
        ]


thirdSection : Model -> Element Msg
thirdSection model =
    row [ width fill ]
        [ paragraph
            [ Font.center
            , R10.Color.AttrsFont.normal model.theme
            , width fill
            ]
            [ column
                [ spacing 10 ]
                [ el [ centerX, Font.size 50 ] <| text (THome.thirdSectionTitle model.lang)
                , el [ centerX, Font.size 40, paddingXY 25 0 ] <| text (THome.thirdSectionBody model.lang)
                ]
            ]
        , image [ width fill ] { src = "images/chat-homepage.jpg", description = "chat" }
        ]


fourthSection : Model -> Element Msg
fourthSection model =
    column
        [ width fill, paddingXY 0 100, spacing 50 ]
        [ paragraph
            [ Font.center
            , paddingXY 50 0
            , R10.Color.AttrsFont.normal model.theme
            ]
            [ column [ spacing 20 ]
                [ el [ Font.size 50, centerX ] <| text (THome.fourthSectionTitle model.lang)
                , el [ Font.size 40, centerX ] <| text (THome.fourthSectionBody model.lang)
                ]
            ]
        , column
            [ centerX
            , paddingXY 30 0
            ]
            (List.map (rowContributors model) (allContributors model.lang))
        ]


allContributors : R10.Language.Language -> List (List ( String, String ))
allContributors lang =
    [ [ ( "Lilian FAVRE GARCIA", THome.managerRole lang )
      , ( "Vincent MOUILLON", THome.architectRole lang )
      , ( "Tristan BONNEAU", THome.developerRole lang )
      , ( "Alexis LEBRUN", THome.developerRole lang )
      ]
    , [ ( "Adrien CHAHINIAN", THome.developerRole lang )
      , ( "Diogo BRANCO GABRIEL", THome.developerRole lang )
      , ( "Stanislas DE RICHTER", THome.developerRole lang )
      , ( "Gaspard THEVENOT", THome.communicationManagerRole lang )
      ]
    ]


rowContributors : Model -> List ( String, String ) -> Element Msg
rowContributors model contributors =
    row [] <| List.map (singleContributor model) contributors


singleContributor : Model -> ( String, String ) -> Element Msg
singleContributor model contributor =
    let
        ( name, role ) =
            contributor
    in
    column [ centerX, padding 10 ]
        [ profileImage
        , profileName model name
        , profileDescription model role
        ]


profileImage : Element Msg
profileImage =
    el
        [ width fill ]
        (image
            [ centerX
            , width (fill |> maximum 225)
            ]
            { src = "images/profile-icon.png"
            , description = "profile picture"
            }
        )


profileName : Model -> String -> Element Msg
profileName model name =
    paragraph
        [ Font.size 20
        , Font.center
        , R10.Color.AttrsFont.normal model.theme
        ]
        [ text name ]


profileDescription : Model -> String -> Element Msg
profileDescription model description =
    paragraph
        [ Font.size 18
        , Font.center
        , R10.Color.AttrsFont.normal model.theme
        ]
        [ text description ]
