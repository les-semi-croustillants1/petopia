module Pages.Lang_String.Wiki.Top exposing (Model, Msg, Params, page)

import Color
import Components.Wiki.Articles as Articles
import Components.Wiki.DropDown as DropDown
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import R10.Button
import R10.Color.AttrsBackground
import R10.Color.AttrsBorder
import R10.Color.AttrsFont
import R10.Color.Utils
import R10.Form
import R10.FormTypes
import R10.Language
import R10.Libu
import R10.Mode
import R10.Theme
import Shared
import Spa.Document exposing (Document)
import Spa.Generated.Route as Route exposing (Route)
import Spa.Page as Page exposing (Page)
import Spa.Url as Url exposing (Url)
import Utils.Language
import Utils.Translations.Wiki as TWiki


page : Page Params Model Msg
page =
    Page.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , save = save
        , load = load
        }



-- INIT


type alias Params =
    { lang : String }


type alias Model =
    { theme : R10.Theme.Theme
    , lang : R10.Language.Language
    , dropDowns : DropDown.Model
    }


init : Shared.Model -> Url Params -> ( Model, Cmd Msg )
init shared { params } =
    ( { theme = shared.theme
      , lang = Utils.Language.fromStringwithDefault params.lang
      , dropDowns = DropDown.init
      }
    , Cmd.none
    )



-- UPDATE


type Msg
    = DropDownMsg DropDown.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        DropDownMsg message ->
            let
                ( newModel, cmd ) =
                    DropDown.update message model.dropDowns
            in
            ( { model | dropDowns = newModel }
            , Cmd.map DropDownMsg cmd
            )


save : Model -> Shared.Model -> Shared.Model
save model shared =
    shared


load : Shared.Model -> Model -> ( Model, Cmd Msg )
load shared model =
    ( { model | theme = shared.theme }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


view : Model -> Document Msg
view model =
    { title = TWiki.wikiTitle model.lang
    , body =
        [ row
            -- main page
            [ width fill
            , spacing 20
            , R10.Color.AttrsBackground.background model.theme
            ]
            [ column
                -- wiki
                [ width <| fillPortion 4
                , height fill
                , spacing 50
                , R10.Color.AttrsFont.normal model.theme
                ]
                [ map DropDownMsg <|
                    DropDown.view { dropdowns = model.dropDowns, lang = model.lang, theme = model.theme }
                , row
                    -- img
                    [ width (fill |> maximum 800)
                    , centerX
                    ]
                    [ petImage
                    ]
                , row
                    -- paragraph
                    ([ width (fill |> maximum 1000)
                     , centerX
                     , padding 30
                     , R10.Color.AttrsBackground.background model.theme
                     , Border.rounded 15
                     , R10.Color.AttrsFont.normal model.theme
                     ]
                        ++ (if R10.Mode.isLight model.theme.mode then
                                [ R10.Color.AttrsBorder.normal model.theme
                                , Border.shadow { offset = ( 1, 1 ), blur = 10, size = 2, color = R10.Color.Utils.fromColorColor Color.darkGray }
                                , Border.width 2
                                ]

                            else
                                [ R10.Color.AttrsBorder.normal model.theme
                                , Border.width 3
                                ]
                           )
                    )
                    [ descriptionParagraph model.theme
                    ]
                ]

            --, Articles.view model.theme
            ]
        ]
    }


petImage : Element Msg
petImage =
    el
        [ width Element.fill ]
        (image
            [ width <| px 400
            , height <| px 300
            , centerX
            , padding 10
            ]
            { src = "https://cdn.dribbble.com/users/113499/screenshots/10356569/media/91abdbcc93cde260d104535623668aef.png?compress=1&resize=800x600"
            , description = "A cat"
            }
        )


descriptionParagraph : R10.Theme.Theme -> Element Msg
descriptionParagraph theme =
    paragraph
        [ padding 50
        , R10.Color.AttrsBackground.background theme
        ]
        [ text "Vous trouverez ici toutes les informations concernant les animaux"
        ]
