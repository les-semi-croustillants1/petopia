module Pages.Lang_String.Wiki.Animal_String.Breed_String exposing (Model, Msg, Params, page)

import Api.Markdown
import Api.Markdown.Data exposing (Data)
import Color
import Components.Wiki.Articles as Articles
import Components.Wiki.DropDown as DropDown
import Components.Wiki.Markdown as Markdown
import Element exposing (..)
import Element.Border as Border
import Element.Font as Font
import Html.Attributes
import R10.Color.AttrsBackground
import R10.Color.AttrsBorder
import R10.Color.AttrsFont
import R10.Color.Utils
import R10.Language
import R10.Mode
import R10.Theme
import Shared
import Spa.Document exposing (Document)
import Spa.Page as Page exposing (Page)
import Spa.Url exposing (Url)
import Utils.Language
import Utils.Translations.Wiki as TWiki
import Utils.Wiki as Wiki


page : Page Params Model Msg
page =
    Page.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , save = save
        , load = load
        }



-- INIT


type alias Params =
    { lang : String
    , animal : String
    , breed : String
    }


type alias Model =
    { breed : String
    , animal : String
    , content : Data String
    , theme : R10.Theme.Theme
    , lang : R10.Language.Language
    , dropDowns : DropDown.Model
    }


init : Shared.Model -> Url Params -> ( Model, Cmd Msg )
init shared { params } =
    let
        lang =
            Utils.Language.fromStringwithDefault params.lang
    in
    ( { breed = Wiki.getTlBreed lang params.animal params.breed
      , animal = Wiki.getTlAnimal lang params.animal
      , content = Api.Markdown.Data.Loading
      , theme = shared.theme
      , lang = lang
      , dropDowns = DropDown.init
      }
    , Api.Markdown.get
        { file = "wiki/" ++ params.breed ++ "-" ++ R10.Language.toStringShort lang ++ ".md"
        , onResponse = GotMarkdown
        }
    )



-- UPDATE


type Msg
    = GotMarkdown (Data String)
    | DropDownMsg DropDown.Msg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotMarkdown content ->
            ( { model | content = content }
            , Cmd.none
            )

        DropDownMsg message ->
            let
                ( newModel, cmd ) =
                    DropDown.update message model.dropDowns
            in
            ( { model | dropDowns = newModel }
            , Cmd.map DropDownMsg cmd
            )


save : Model -> Shared.Model -> Shared.Model
save _ shared =
    shared


load : Shared.Model -> Model -> ( Model, Cmd Msg )
load shared model =
    ( { model | theme = shared.theme }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none



-- VIEW


view : Model -> Document Msg
view model =
    { title = TWiki.wikiTitle model.lang ++ " - " ++ model.animal ++ " - " ++ model.breed
    , body =
        [ row
            -- main page
            [ width fill
            , spacing 20
            , R10.Color.AttrsBackground.background model.theme
            ]
            [ column
                -- wiki
                [ width <| fillPortion 4
                , height fill
                , spacing 20
                , R10.Color.AttrsFont.normal model.theme
                ]
                [ map DropDownMsg <|
                    DropDown.view { dropdowns = model.dropDowns, lang = model.lang, theme = model.theme }
                , row
                    --titles
                    [ width fill
                    , centerX
                    , R10.Color.AttrsFont.normal model.theme
                    ]
                    [ viewTitles model
                    ]
                , column
                    -- paragraph
                    ([ width (fill |> maximum 1000)
                     , centerX
                     , padding 30
                     , R10.Color.AttrsBackground.background model.theme
                     , R10.Color.AttrsBackground.background model.theme
                     , Border.rounded 15
                     , R10.Color.AttrsFont.normal model.theme
                     ]
                        ++ (if R10.Mode.isLight model.theme.mode then
                                [ R10.Color.AttrsBorder.normal model.theme
                                , Border.shadow
                                    { offset = ( 1, 1 )
                                    , blur = 10
                                    , size = 2
                                    , color = R10.Color.Utils.fromColorColor Color.darkGray
                                    }
                                , Border.width 2
                                ]

                            else
                                [ R10.Color.AttrsBorder.normal model.theme
                                , Border.width 3
                                ]
                           )
                    )
                    [ descriptionParagraph model ]
                ]

            --, Articles.view model.theme
            ]
        ]
    }


descriptionParagraph : Model -> Element Msg
descriptionParagraph model =
    paragraph
        []
        [ html <|
            Api.Markdown.Data.view
                Markdown.view
                model.content
        ]


viewTitles : Model -> Element Msg
viewTitles model =
    column
        [ centerX ]
        [ el
            [ Font.bold
            , Font.size 60
            , padding 10
            , centerX
            , htmlAttribute <| Html.Attributes.style "text-transform" "capitalize"
            ]
          <|
            text model.animal
        , el
            [ Font.size 50
            , centerX
            , htmlAttribute <| Html.Attributes.style "text-transform" "capitalize"
            ]
          <|
            text model.breed
        ]
