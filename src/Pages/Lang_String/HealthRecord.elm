module Pages.Lang_String.HealthRecord exposing (Model, Msg, Params, page)

import Api
import Api.Data as Data
import Api.DeleteBooklet as DeleteBooklet
import Api.GetBooklet as GetBooklet
import Api.GetUserBooklets as GetUserBooklets
import Api.InsertBooklet as InsertBooklet
import Api.UpdateBooklet as UpdateBooklet
import Dialog
import Dict exposing (Dict)
import Element exposing (..)
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Graphql.Http
import Html
import Html.Attributes as Attributes
import Maybe exposing (Maybe)
import PetopiaDB.Scalar
import R10.Button
import R10.Color.AttrsBackground
import R10.Color.AttrsBorder
import R10.Color.AttrsFont
import R10.Color.Svg
import R10.Form
import R10.FormTypes
import R10.Language exposing (Language(..))
import R10.Libu
import R10.Svg.Icons
import R10.Theme
import RemoteData exposing (RemoteData)
import Shared
import Spa.Document exposing (Document)
import Spa.Page as Page exposing (Page)
import Spa.Url as Url exposing (Url)
import Utils.Language
import Utils.Translations.HealthRecord as THealthRecord


page : Page Params Model Msg
page =
    Page.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , save = save
        , load = load
        }



-- INIT


type alias Params =
    { lang : String }


type alias Model =
    { lang : R10.Language.Language
    , theme : R10.Theme.Theme
    , bookletId : Maybe Int
    , listSimpleBooklets : List SimpleBooklet
    , dictBooklets : Dict Int Data.Booklet
    , editName : Bool
    , editGeneralInfo : Bool
    , editIdentification : Bool
    , editMedicalInformation : Bool
    , editVaccineInformation : Bool
    , editNotes : Bool
    , generalInfoForm : R10.Form.Form
    , identificationForm : R10.Form.Form
    , medicalInfoForm : R10.Form.Form
    , vaccineInfoForm : R10.Form.Form
    , notesForm : R10.Form.Form
    , showPopup : Bool
    , token : Maybe String
    }


type alias SimpleBooklet =
    { inMemory : Bool
    , booklet : GetUserBooklets.Booklet
    }


type FormType
    = GeneralInfoForm
    | IdentificationForm
    | MedicalInfoForm
    | VaccineInfoForm
    | NotesForm


init : Shared.Model -> Url Params -> ( Model, Cmd Msg )
init shared { params } =
    let
        lang =
            Utils.Language.fromStringwithDefault params.lang
    in
    ( { lang = lang
      , theme = shared.theme
      , bookletId = Nothing
      , listSimpleBooklets = []
      , dictBooklets = Dict.empty
      , editName = False
      , editGeneralInfo = False
      , editIdentification = False
      , editMedicalInformation = False
      , editVaccineInformation = False
      , editNotes = False
      , generalInfoForm = initForm lang GeneralInfoForm
      , identificationForm = initForm lang IdentificationForm
      , medicalInfoForm = initForm lang MedicalInfoForm
      , notesForm = initForm lang NotesForm
      , vaccineInfoForm = initForm lang VaccineInfoForm
      , showPopup = False
      , token = shared.token
      }
    , Api.getUserBooklets GetUserBooklets (Maybe.withDefault "" shared.token)
    )



-- UPDATE


type Msg
    = GetUserBooklets (RemoteData (Graphql.Http.Error GetUserBooklets.Data) GetUserBooklets.Data)
    | GetBooklet Int (RemoteData (Graphql.Http.Error GetBooklet.Data) GetBooklet.Data)
    | UpdateBooklet Int (RemoteData (Graphql.Http.Error UpdateBooklet.Data) UpdateBooklet.Data)
    | DeleteBooklet (RemoteData (Graphql.Http.Error DeleteBooklet.Data) DeleteBooklet.Data)
    | InsertBooklet Int (RemoteData (Graphql.Http.Error InsertBooklet.Data) InsertBooklet.Data)
    | OnClickAnimal SimpleBooklet
    | OnClickAdd
    | OnClickEdit FormType
    | FormMsg FormType Int R10.Form.Msg
    | OnClickDelete Int
    | AskDeletion
    | ClosePopup


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GetUserBooklets result ->
            ( case result of
                RemoteData.Failure _ ->
                    model

                RemoteData.Success data ->
                    let
                        convert booklet =
                            SimpleBooklet False booklet
                    in
                    { model | listSimpleBooklets = List.map convert data.booklets }

                _ ->
                    model
            , Cmd.none
            )

        GetBooklet id result ->
            ( case result of
                RemoteData.Failure _ ->
                    model

                RemoteData.Success data ->
                    case data.booklet of
                        Nothing ->
                            model

                        Just booklet ->
                            let
                                generalInfoForm =
                                    model.generalInfoForm

                                identificationForm =
                                    model.identificationForm

                                medicalInfoForm =
                                    model.medicalInfoForm

                                notesForm =
                                    model.notesForm
                            in
                            { model
                                | bookletId = Just id
                                , dictBooklets = Dict.insert id booklet model.dictBooklets
                                , generalInfoForm = setFormValues generalInfoForm GeneralInfoForm booklet
                                , identificationForm = setFormValues identificationForm IdentificationForm booklet
                                , medicalInfoForm = setFormValues medicalInfoForm MedicalInfoForm booklet
                                , notesForm = setFormValues notesForm NotesForm booklet
                            }

                _ ->
                    model
            , Cmd.none
            )

        DeleteBooklet result ->
            ( case result of
                RemoteData.Failure _ ->
                    { model | bookletId = Nothing }

                RemoteData.Success data ->
                    case data.booklet of
                        Nothing ->
                            model

                        Just booklet ->
                            let
                                filteredSimpleList =
                                    List.filter (\b -> b.booklet.id /= booklet.id) model.listSimpleBooklets

                                filteredDict =
                                    Dict.remove booklet.id model.dictBooklets
                            in
                            { model
                                | bookletId = Nothing
                                , dictBooklets = filteredDict
                                , listSimpleBooklets = filteredSimpleList
                            }

                _ ->
                    model
            , Cmd.none
            )

        InsertBooklet oldId result ->
            ( case result of
                RemoteData.Failure _ ->
                    model

                RemoteData.Success data ->
                    case data.booklet of
                        Nothing ->
                            model

                        Just bookletId ->
                            let
                                booklet =
                                    Maybe.withDefault defaultBooklet (Dict.get oldId model.dictBooklets)

                                newId =
                                    bookletId.id

                                filteredDict =
                                    Dict.remove oldId model.dictBooklets

                                newDict =
                                    Dict.insert newId booklet filteredDict

                                newSimpleList =
                                    modifySimpleList oldId newId booklet.name model.listSimpleBooklets
                            in
                            { model
                                | dictBooklets = newDict
                                , listSimpleBooklets = newSimpleList
                            }

                _ ->
                    model
            , Cmd.none
            )

        UpdateBooklet id result ->
            ( case result of
                RemoteData.Failure _ ->
                    model

                RemoteData.Success data ->
                    case data.booklet of
                        Nothing ->
                            model

                        Just _ ->
                            model

                -- Don't know what to do
                _ ->
                    model
            , Cmd.none
            )

        OnClickAnimal simpleBooklet ->
            if simpleBooklet.inMemory then
                let
                    booklet =
                        Maybe.withDefault
                            defaultBooklet
                            (Dict.get simpleBooklet.booklet.id model.dictBooklets)

                    generalInfoForm =
                        model.generalInfoForm

                    identificationForm =
                        model.identificationForm

                    medicalInfoForm =
                        model.medicalInfoForm

                    notesForm =
                        model.notesForm
                in
                ( { model
                    | bookletId = Just simpleBooklet.booklet.id
                    , editName = False
                    , editGeneralInfo = False
                    , editIdentification = False
                    , editMedicalInformation = False
                    , editVaccineInformation = False
                    , editNotes = False
                    , generalInfoForm = setFormValues generalInfoForm GeneralInfoForm booklet
                    , identificationForm = setFormValues identificationForm IdentificationForm booklet
                    , medicalInfoForm = setFormValues medicalInfoForm MedicalInfoForm booklet
                    , vaccineInfoForm = initForm model.lang VaccineInfoForm
                    , notesForm = setFormValues notesForm NotesForm booklet
                  }
                , Cmd.none
                )

            else
                ( model
                , Api.getBooklet simpleBooklet.booklet.id (GetBooklet simpleBooklet.booklet.id) (Maybe.withDefault "" model.token)
                )

        OnClickAdd ->
            ( { model
                | bookletId = Just (0 - List.length model.listSimpleBooklets)
                , editName = True
                , editGeneralInfo = True
                , editIdentification = True
                , editMedicalInformation = True
                , editVaccineInformation = False
                , editNotes = True
                , vaccineInfoForm = initForm model.lang VaccineInfoForm
                , generalInfoForm = initForm model.lang GeneralInfoForm
                , identificationForm = initForm model.lang IdentificationForm
                , medicalInfoForm = initForm model.lang MedicalInfoForm
                , notesForm = initForm model.lang NotesForm
              }
            , Cmd.none
            )

        OnClickEdit formType ->
            ( case formType of
                GeneralInfoForm ->
                    { model | editGeneralInfo = not model.editGeneralInfo }

                IdentificationForm ->
                    { model | editIdentification = not model.editIdentification }

                MedicalInfoForm ->
                    { model | editMedicalInformation = not model.editMedicalInformation }

                VaccineInfoForm ->
                    { model | editVaccineInformation = not model.editVaccineInformation }

                NotesForm ->
                    { model | editNotes = not model.editNotes }
            , Cmd.none
            )

        AskDeletion ->
            ( { model | showPopup = True }
            , Cmd.none
            )

        OnClickDelete id ->
            ( { model | showPopup = False }
            , Api.deleteBooklet id DeleteBooklet (Maybe.withDefault "" model.token)
            )

        ClosePopup ->
            ( { model | showPopup = False }
            , Cmd.none
            )

        FormMsg formType id r10FormMsg ->
            let
                booklet =
                    Maybe.withDefault defaultBooklet (Dict.get id model.dictBooklets)

                form =
                    case formType of
                        GeneralInfoForm ->
                            model.generalInfoForm

                        IdentificationForm ->
                            model.identificationForm

                        MedicalInfoForm ->
                            model.medicalInfoForm

                        VaccineInfoForm ->
                            model.vaccineInfoForm

                        NotesForm ->
                            model.notesForm

                formUpdate =
                    R10.Form.update r10FormMsg form.state

                newState =
                    Tuple.first formUpdate

                cmd =
                    Cmd.map (FormMsg formType id) (Tuple.second formUpdate)

                newForm =
                    { form | state = newState }

                newBooklet =
                    newBookletFromMsg booklet newForm

                updatedFormModel =
                    case formType of
                        GeneralInfoForm ->
                            { model | generalInfoForm = newForm }

                        IdentificationForm ->
                            { model | identificationForm = newForm }

                        MedicalInfoForm ->
                            { model | medicalInfoForm = newForm }

                        VaccineInfoForm ->
                            { model | vaccineInfoForm = newForm }

                        NotesForm ->
                            { model | notesForm = newForm }
            in
            if R10.Form.isFormSubmittableAndSubmitted newForm r10FormMsg then
                let
                    newDict =
                        Dict.update id (\_ -> Just newBooklet) model.dictBooklets

                    newSimpleList =
                        modifySimpleList id id newBooklet.name model.listSimpleBooklets

                    cmdUpdate =
                        Api.updateBooklet
                            (UpdateBooklet.UpdateBookletByPkInput
                                id
                                (Just newBooklet.name)
                                (Just newBooklet.birth_date)
                                (Just newBooklet.sex)
                                (Just newBooklet.color)
                                newBooklet.breed
                                newBooklet.tatoo_number
                                newBooklet.electronic_id
                                newBooklet.diseases
                                newBooklet.treatment
                                newBooklet.notes
                            )
                            (UpdateBooklet id)
                            (Maybe.withDefault "" model.token)

                    cmdInsert =
                        Api.insertBooklet
                            (InsertBooklet.InsertBookletOneInput
                                newBooklet.name
                                newBooklet.birth_date
                                newBooklet.sex
                                newBooklet.color
                                newBooklet.breed
                                newBooklet.tatoo_number
                                newBooklet.electronic_id
                                newBooklet.diseases
                                newBooklet.treatment
                                newBooklet.notes
                            )
                            (InsertBooklet id)
                            (Maybe.withDefault "" model.token)

                    submitCmd =
                        Cmd.batch <|
                            cmd
                                :: (if id > 0 then
                                        [ cmdUpdate ]

                                    else
                                        case formType of
                                            GeneralInfoForm ->
                                                [ cmdInsert ]

                                            _ ->
                                                [ Cmd.none ]
                                   )

                    newModel =
                        { updatedFormModel
                            | bookletId = Just id
                            , dictBooklets = newDict
                            , listSimpleBooklets = newSimpleList
                        }
                in
                ( case formType of
                    GeneralInfoForm ->
                        { newModel | editGeneralInfo = not model.editGeneralInfo }

                    IdentificationForm ->
                        { newModel | editIdentification = not model.editIdentification }

                    MedicalInfoForm ->
                        { newModel | editMedicalInformation = not model.editMedicalInformation }

                    VaccineInfoForm ->
                        { newModel | editVaccineInformation = not model.editVaccineInformation }

                    NotesForm ->
                        { newModel | editNotes = not model.editNotes }
                , submitCmd
                )

            else
                ( updatedFormModel, cmd )


setFormValues : R10.Form.Form -> FormType -> Data.Booklet -> R10.Form.Form
setFormValues form formType booklet =
    { form
        | state =
            case formType of
                GeneralInfoForm ->
                    let
                        birth_date =
                            case booklet.birth_date of
                                PetopiaDB.Scalar.Date date ->
                                    date
                    in
                    R10.Form.setFieldValue "name" booklet.name <|
                        R10.Form.setFieldValue "dob" birth_date <|
                            R10.Form.setFieldValue "breed" (Maybe.withDefault "" booklet.breed) <|
                                R10.Form.setFieldValue "color" booklet.color <|
                                    R10.Form.setFieldValue "sex" booklet.sex R10.Form.initState

                IdentificationForm ->
                    R10.Form.setFieldValue "tattooNb" (Maybe.withDefault "" booklet.tatoo_number) <|
                        R10.Form.setFieldValue "electronicId" (Maybe.withDefault "" booklet.electronic_id) R10.Form.initState

                MedicalInfoForm ->
                    R10.Form.setFieldValue "diseases" (Maybe.withDefault "" booklet.diseases) <|
                        R10.Form.setFieldValue "vaccines" "" <|
                            R10.Form.setFieldValue "treatments" (Maybe.withDefault "" booklet.treatment) R10.Form.initState

                NotesForm ->
                    R10.Form.setFieldValue "notes" (Maybe.withDefault "" booklet.notes) R10.Form.initState

                _ ->
                    R10.Form.initState
    }


modifySimpleList : Int -> Int -> String -> List SimpleBooklet -> List SimpleBooklet
modifySimpleList oldId newId name list =
    let
        filteredSimpleList =
            List.filter (\b -> b.booklet.id /= oldId) list

        newSimpleList =
            List.sortBy (\elt -> elt.booklet.id) <|
                filteredSimpleList
                    ++ [ SimpleBooklet True (GetUserBooklets.Booklet newId name) ]

        filteredNewSimpleList =
            List.filter (\b -> b.booklet.id < 0) newSimpleList

        reversedNewSimpleList =
            List.reverse filteredNewSimpleList
    in
    List.drop (List.length reversedNewSimpleList) newSimpleList
        ++ reversedNewSimpleList


newBookletFromMsg : Data.Booklet -> R10.Form.Form -> Data.Booklet
newBookletFromMsg oldBooklet form =
    let
        newName =
            Maybe.withDefault
                oldBooklet.name
                (R10.Form.getFieldValue "name" form.state)

        dobString =
            case oldBooklet.birth_date of
                PetopiaDB.Scalar.Date date ->
                    date

        newDOB =
            PetopiaDB.Scalar.Date <|
                Maybe.withDefault
                    dobString
                    (R10.Form.getFieldValue "dob" form.state)

        newBreed =
            R10.Form.getFieldValue "breed" form.state

        newColor =
            Maybe.withDefault
                oldBooklet.color
                (R10.Form.getFieldValue "color" form.state)

        newSex =
            Maybe.withDefault
                oldBooklet.sex
                (R10.Form.getFieldValue "sex" form.state)

        newTattooNb =
            R10.Form.getFieldValue "tattooNb" form.state

        newElecId =
            R10.Form.getFieldValue "electronicId" form.state

        newDiseases =
            R10.Form.getFieldValue "diseases" form.state

        newTreatments =
            R10.Form.getFieldValue "treatments" form.state

        newVaccineName =
            Maybe.withDefault "" (R10.Form.getFieldValue "vaccineName" form.state)

        newVaccineSpot =
            R10.Form.getFieldValue "injectionSpot" form.state

        newVaccineNumber =
            R10.Form.getFieldValue "serialNumber" form.state

        newVeterinary =
            R10.Form.getFieldValue "veterinary" form.state

        newVaccine =
            Data.Vaccine
                (PetopiaDB.Scalar.Date "")
                newVaccineSpot
                newVaccineName
                newVaccineNumber

        newAnimal =
            Data.Animal
                oldBooklet.animal.vaccines

        newNotes =
            R10.Form.getFieldValue "notes" form.state
    in
    Data.Booklet
        newDOB
        newBreed
        newColor
        newDiseases
        newElecId
        newName
        newNotes
        newSex
        newTattooNb
        newTreatments
        newAnimal


defaultBooklet : Data.Booklet
defaultBooklet =
    Data.Booklet
        (PetopiaDB.Scalar.Date "")
        Nothing
        ""
        Nothing
        Nothing
        ""
        Nothing
        ""
        Nothing
        Nothing
        (Data.Animal [])


save : Model -> Shared.Model -> Shared.Model
save model shared =
    shared


load : Shared.Model -> Model -> ( Model, Cmd Msg )
load shared model =
    ( { model | theme = shared.theme, token = shared.token }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



-- VIEW


view : Model -> Document Msg
view model =
    { title = THealthRecord.healthRecordTitle model.lang
    , body =
        [ column
            [ width fill
            , height fill
            ]
            [ row
                [ width fill
                , height fill
                , scrollbars
                , inFront <|
                    html <|
                        Html.div []
                            [ Html.node "link"
                                [ Attributes.href "https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
                                , Attributes.rel "stylesheet"
                                ]
                                []
                            , Dialog.view
                                (if model.showPopup then
                                    Just <| popUpDeleteAnimal model

                                 else
                                    Nothing
                                )
                            ]
                ]
                [ viewAnimalSelection model
                , viewHealthRecord model
                ]
            ]
        ]
    }


viewAnimalSelection : Model -> Element Msg
viewAnimalSelection model =
    column
        [ width <| fillPortion 1
        , height fill
        , spacing 20
        , paddingXY 20 20
        , R10.Color.AttrsBackground.background model.theme
        , Border.widthEach { bottom = 0, top = 0, left = 0, right = 2 }
        , R10.Color.AttrsBorder.normal model.theme
        , scrollbars
        ]
    <|
        List.map (viewAnimalSelector model) model.listSimpleBooklets
            ++ [ viewAddNewAnimal model
               ]


viewAnimalSelector : Model -> SimpleBooklet -> Element Msg
viewAnimalSelector { theme, bookletId } simpleBooklet =
    el
        ([ width fill
         , padding 50
         , Font.center
         , Font.size 25
         , Events.onClick <| OnClickAnimal simpleBooklet
         , pointer
         , Border.rounded 15
         , R10.Color.AttrsBackground.background theme
         ]
            ++ (case bookletId of
                    Nothing ->
                        [ R10.Color.AttrsBorder.normal theme
                        , Border.width 2
                        ]

                    Just id ->
                        if id == simpleBooklet.booklet.id then
                            [ Border.width 3
                            , R10.Color.AttrsBorder.inputFieldCheckboxSelected theme
                            ]

                        else
                            [ R10.Color.AttrsBorder.normal theme
                            , Border.width 2
                            ]
               )
        )
    <|
        paragraph
            [ R10.Color.AttrsFont.normal theme ]
            [ text simpleBooklet.booklet.name ]


viewAddNewAnimal : Model -> Element Msg
viewAddNewAnimal { theme } =
    R10.Svg.Icons.plus
        [ centerX
        , Events.onClick OnClickAdd
        , pointer
        , padding 50
        ]
        (R10.Color.Svg.primary theme)
        60


viewHealthRecord : Model -> Element Msg
viewHealthRecord model =
    column
        [ width <| fillPortion 3
        , height fill
        , scrollbars
        , R10.Color.AttrsBackground.background model.theme
        , padding 50
        , spacing 20
        ]
        (case model.bookletId of
            Just id ->
                List.map
                    (\viewFunction -> viewFunction model id)
                    [ viewName
                    , viewGeneralInfo
                    , viewIdentification
                    , viewMedicalInformation
                    , viewVaccinesInfo
                    , viewNotes
                    ]
                    ++ [ R10.Button.primary
                            [ width shrink
                            , alignRight
                            ]
                            { label = text <| THealthRecord.deleteAnimalButton model.lang
                            , libu = R10.Libu.Bu <| Just AskDeletion
                            , theme = model.theme
                            }
                       ]

            Nothing ->
                []
        )


viewName : Model -> Int -> Element Msg
viewName { theme, dictBooklets } id =
    let
        booklet =
            Maybe.withDefault defaultBooklet (Dict.get id dictBooklets)
    in
    el
        [ Font.size 50
        , width fill
        , Border.widthEach { bottom = 1, top = 0, left = 0, right = 0 }
        , paddingXY 0 30
        , R10.Color.AttrsFont.normal theme
        ]
    <|
        text booklet.name


viewGeneralInfo : Model -> Int -> Element Msg
viewGeneralInfo { theme, lang, editGeneralInfo, generalInfoForm, dictBooklets } id =
    let
        booklet =
            Maybe.withDefault defaultBooklet (Dict.get id dictBooklets)
    in
    column
        (viewAttrs GeneralInfoForm theme)
    <|
        rowTitleButton THealthRecord.generalInfoSectionTitle GeneralInfoForm id generalInfoForm.conf lang theme editGeneralInfo
            :: (if editGeneralInfo == True then
                    R10.Form.viewWithOptions
                        generalInfoForm
                        (FormMsg GeneralInfoForm id)
                        (R10.Form.Options
                            Nothing
                            Nothing
                            R10.Form.style.filled
                            (Just <| R10.Form.themeToPalette theme)
                        )

                else
                    let
                        dob =
                            case booklet.birth_date of
                                PetopiaDB.Scalar.Date date ->
                                    date
                    in
                    [ rowSingleInfo (THealthRecord.dobLabel lang False) dob
                    , rowSingleInfo (THealthRecord.breedLabel lang False) (Maybe.withDefault "" booklet.breed)
                    , rowSingleInfo (THealthRecord.colorLabel lang False) booklet.color
                    , rowSingleInfo (THealthRecord.sexLabel lang False) booklet.sex
                    ]
               )


viewIdentification : Model -> Int -> Element Msg
viewIdentification { theme, lang, editIdentification, identificationForm, dictBooklets } id =
    let
        booklet =
            Maybe.withDefault defaultBooklet (Dict.get id dictBooklets)
    in
    column
        (viewAttrs IdentificationForm theme)
    <|
        rowTitleButton THealthRecord.identificationSectionTitle IdentificationForm id identificationForm.conf lang theme editIdentification
            :: (if editIdentification == True then
                    R10.Form.viewWithOptions
                        identificationForm
                        (FormMsg IdentificationForm id)
                        (R10.Form.Options
                            Nothing
                            Nothing
                            R10.Form.style.filled
                            (Just <| R10.Form.themeToPalette theme)
                        )

                else
                    [ rowSingleInfo (THealthRecord.tattooNbLabel lang False) (Maybe.withDefault "" booklet.tatoo_number)
                    , rowSingleInfo (THealthRecord.electonicIdLabel lang False) (Maybe.withDefault "" booklet.electronic_id)
                    ]
               )


viewMedicalInformation : Model -> Int -> Element Msg
viewMedicalInformation { theme, lang, editMedicalInformation, medicalInfoForm, dictBooklets } id =
    let
        booklet =
            Maybe.withDefault defaultBooklet (Dict.get id dictBooklets)
    in
    column
        (viewAttrs MedicalInfoForm theme)
    <|
        rowTitleButton THealthRecord.medicalInfoSectionTitle MedicalInfoForm id medicalInfoForm.conf lang theme editMedicalInformation
            :: (if editMedicalInformation == True then
                    R10.Form.viewWithOptions
                        medicalInfoForm
                        (FormMsg MedicalInfoForm id)
                        (R10.Form.Options
                            Nothing
                            Nothing
                            R10.Form.style.filled
                            (Just <| R10.Form.themeToPalette theme)
                        )

                else
                    [ rowSingleInfo (THealthRecord.diseasesLabel lang False) (Maybe.withDefault "" booklet.diseases)
                    , rowSingleInfo (THealthRecord.treatmentsLabel lang False) (Maybe.withDefault "" booklet.treatment)
                    ]
               )


viewVaccinesInfo : Model -> Int -> Element Msg
viewVaccinesInfo { theme, lang, editVaccineInformation, vaccineInfoForm, dictBooklets } id =
    let
        booklet =
            Maybe.withDefault defaultBooklet (Dict.get id dictBooklets)
    in
    column
        (viewAttrs VaccineInfoForm theme)
    <|
        [ rowTitleButton THealthRecord.vaccinesSectionTitle VaccineInfoForm id vaccineInfoForm.conf lang theme editVaccineInformation
        , row
            [ padding 10, width fill ]
            (if List.length booklet.animal.vaccines > 0 then
                [ viewVaccine lang booklet.animal.vaccines ]

             else
                []
            )
        ]
            ++ (if editVaccineInformation == True then
                    R10.Form.viewWithOptions
                        vaccineInfoForm
                        (FormMsg VaccineInfoForm id)
                        (R10.Form.Options
                            Nothing
                            Nothing
                            R10.Form.style.filled
                            (Just <| R10.Form.themeToPalette theme)
                        )

                else
                    []
               )


viewVaccine : R10.Language.Language -> List Data.Vaccine -> Element Msg
viewVaccine lang vaccines =
    let
        tableAttrs =
            [ width fill
            , alignTop
            , Border.widthEach { bottom = 0, left = 0, right = 1, top = 1 }
            ]

        cellAttrs =
            [ padding 10
            , height fill
            , Font.center
            , Border.widthEach { bottom = 1, left = 1, right = 0, top = 0 }
            ]

        headerAttrs =
            Font.bold :: cellAttrs

        header t =
            el headerAttrs <| text t

        cell t =
            el cellAttrs <| text t
    in
    indexedTable
        tableAttrs
        { data = vaccines
        , columns =
            [ { header = header <| THealthRecord.vaccinesDateLabel lang True
              , width = fill
              , view =
                    \_ vaccine ->
                        let
                            date =
                                case vaccine.date of
                                    PetopiaDB.Scalar.Date val ->
                                        val
                        in
                        cell date
              }
            , { header = header <| THealthRecord.vaccinesNameLabel lang True
              , width = fill
              , view = \_ vaccine -> cell vaccine.name
              }
            , { header = header <| THealthRecord.vaccinesInjectionSpotLabel lang True
              , width = fill
              , view = \_ vaccine -> cell (Maybe.withDefault "" vaccine.injection_spot)
              }
            , { header = header <| THealthRecord.vaccinesSerialNbLabel lang True
              , width = fill
              , view = \_ vaccine -> cell (Maybe.withDefault "" vaccine.serial_num)
              }
            , { header = header <| THealthRecord.vaccinesVeterinaryLabel lang True
              , width = fill
              , view = \_ vaccine -> cell vaccine.veterinary.username
              }
            ]
        }


viewNotes : Model -> Int -> Element Msg
viewNotes { theme, lang, editNotes, notesForm, dictBooklets } id =
    let
        booklet =
            Maybe.withDefault defaultBooklet (Dict.get id dictBooklets)
    in
    column
        (viewAttrs NotesForm theme)
    <|
        rowTitleButton THealthRecord.notesSectionTitle NotesForm id notesForm.conf lang theme editNotes
            :: (if editNotes == True then
                    R10.Form.viewWithTheme notesForm (FormMsg NotesForm id) theme

                else
                    [ paragraph [ padding 10, width fill ]
                        [ text (Maybe.withDefault "" booklet.notes)
                        ]
                    ]
               )


viewAttrs : FormType -> R10.Theme.Theme -> List (Attribute Msg)
viewAttrs formType theme =
    [ spacing 20
    , width fill
    , paddingXY 0 10
    , R10.Color.AttrsFont.normal theme
    ]
        ++ (case formType of
                NotesForm ->
                    []

                _ ->
                    [ Border.widthEach { bottom = 1, top = 0, left = 0, right = 0 } ]
           )


rowSingleInfo : String -> String -> Element Msg
rowSingleInfo label value =
    row [ padding 10 ]
        [ text label
        , text value
        ]


rowTitleButton : (R10.Language.Language -> String) -> FormType -> Int -> R10.Form.Conf -> R10.Language.Language -> R10.Theme.Theme -> Bool -> Element Msg
rowTitleButton title formType bookletId formConf lang theme editing =
    row
        [ spacing 30, width fill ]
        ((el
            [ Font.size 35
            , R10.Color.AttrsFont.normal theme
            ]
          <|
            text <|
                title lang
         )
            :: (case formType of
                    VaccineInfoForm ->
                        []

                    _ ->
                        [ editButton formType bookletId formConf lang theme editing ]
               )
        )


editButton : FormType -> Int -> R10.Form.Conf -> R10.Language.Language -> R10.Theme.Theme -> Bool -> Element Msg
editButton formType bookletId formConf lang theme editing =
    R10.Button.primary
        [ width shrink
        , alignRight
        ]
        { label =
            text
                (if editing then
                    THealthRecord.saveFormAnimalButton lang

                 else
                    THealthRecord.editFormAnimalButton lang
                )
        , libu =
            R10.Libu.Bu <|
                Just <|
                    if editing then
                        FormMsg formType bookletId (R10.Form.msg.submit formConf)

                    else
                        OnClickEdit formType
        , theme = theme
        }


popUpDeleteAnimal : Model -> Dialog.Config Msg
popUpDeleteAnimal { bookletId, lang, theme } =
    { closeMessage = Just ClosePopup
    , containerClass = Nothing
    , header =
        Just <|
            layout
                [ centerX
                , centerY
                , width shrink
                ]
            <|
                text <|
                    THealthRecord.popUpDeleteAnimalHeader lang
    , body =
        Just <|
            layout [] <|
                text <|
                    THealthRecord.popUpDeleteAnimalBody lang
    , footer =
        [ layout [] <|
            R10.Button.primary
                [ centerX
                , centerY
                , width shrink
                ]
                { label = text <| THealthRecord.popUpDeleteAnimalYesButton lang
                , libu =
                    R10.Libu.Bu <|
                        Just <|
                            OnClickDelete <|
                                Maybe.withDefault 0 bookletId
                , theme = theme
                }
        ]
    }



-- FORMS


initForm : R10.Language.Language -> FormType -> R10.Form.Form
initForm lang formType =
    { conf = conf lang formType
    , state = R10.Form.initState
    }



-- CONF


conf : R10.Language.Language -> FormType -> R10.Form.Conf
conf lang formType =
    List.map
        (\entity -> entity lang)
        (case formType of
            GeneralInfoForm ->
                [ nameEntity
                , dobEntity
                , breedEntity
                , colorEntity
                , sexEntity
                ]

            IdentificationForm ->
                [ tattooNbEntity
                , electronicIdEntity
                ]

            MedicalInfoForm ->
                [ diseasesEntity
                , treatmentsEntity
                ]

            VaccineInfoForm ->
                [ vaccineDateEntity
                , vaccineNameEntity
                , vaccineSpotEntity
                , vaccineNumberEntity
                , vaccineVeterinaryEntity
                ]

            NotesForm ->
                [ notesEntity ]
        )



-- ENTITIES


nameEntity : R10.Language.Language -> R10.Form.Entity
nameEntity lang =
    R10.Form.entity.field
        { id = "name"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.nameLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.commonValidation.alphaNumericDashSpace
                    , R10.Form.validation.maxLength 20
                    , R10.Form.validation.required
                    ]
                }
        }


dobEntity : R10.Language.Language -> R10.Form.Entity
dobEntity lang =
    R10.Form.entity.field
        { id = "dob"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.dobLabel lang True
        , helperText = Just <| THealthRecord.dateFieldHelper lang
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.validation.required
                    , R10.Form.validation.regex "(\\d{4})-((?:0[1-9])|(?:1[0-2]))-((?:0[0-9])|(?:[1-2][0-9])|(?:3[0-1]))"
                    ]
                }
        }


breedEntity : R10.Language.Language -> R10.Form.Entity
breedEntity lang =
    R10.Form.entity.field
        { id = "breed"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.breedLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.commonValidation.alphaNumericDashSpace
                    , R10.Form.validation.maxLength 50
                    , R10.Form.validation.required
                    ]
                }
        }


colorEntity : R10.Language.Language -> R10.Form.Entity
colorEntity lang =
    R10.Form.entity.field
        { id = "color"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.colorLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.commonValidation.alphaNumericDashSpace
                    , R10.Form.validation.maxLength 50
                    , R10.Form.validation.required
                    ]
                }
        }


sexEntity : R10.Language.Language -> R10.Form.Entity
sexEntity lang =
    R10.Form.entity.field
        { id = "sex"
        , idDom = Nothing
        , type_ =
            R10.FormTypes.TypeSingle R10.FormTypes.SingleSelect
                [ { value = "male", label = THealthRecord.sexMaleLabel lang }
                , { value = "female", label = THealthRecord.sexFemaleLabel lang }
                ]
        , label = THealthRecord.sexLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.validation.required ]
                }
        }


tattooNbEntity : R10.Language.Language -> R10.Form.Entity
tattooNbEntity lang =
    R10.Form.entity.field
        { id = "tattooNb"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.tattooNbLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.commonValidation.alphaNumericDashSpace
                    , R10.Form.validation.maxLength 50
                    ]
                }
        }


electronicIdEntity : R10.Language.Language -> R10.Form.Entity
electronicIdEntity lang =
    R10.Form.entity.field
        { id = "electronicId"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.electonicIdLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.commonValidation.alphaNumericDashSpace
                    , R10.Form.validation.maxLength 50
                    ]
                }
        }


diseasesEntity : R10.Language.Language -> R10.Form.Entity
diseasesEntity lang =
    R10.Form.entity.field
        { id = "diseases"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.diseasesLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.commonValidation.alphaNumericDashSpace
                    , R10.Form.validation.maxLength 500
                    ]
                }
        }


treatmentsEntity : R10.Language.Language -> R10.Form.Entity
treatmentsEntity lang =
    R10.Form.entity.field
        { id = "treatments"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.treatmentsLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.commonValidation.alphaNumericDashSpace
                    , R10.Form.validation.maxLength 500
                    ]
                }
        }


vaccineDateEntity : R10.Language.Language -> R10.Form.Entity
vaccineDateEntity lang =
    R10.Form.entity.field
        { id = "vaccineDate"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.vaccinesDateLabel lang True
        , helperText = Just <| THealthRecord.dateFieldHelper lang
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.validation.regex "(\\d{4})-((?:0[1-9])|(?:1[0-2]))-((?:0[0-9])|(?:[1-2][0-9])|(?:3[0-1]))"
                    ]
                }
        }


vaccineNameEntity : R10.Language.Language -> R10.Form.Entity
vaccineNameEntity lang =
    R10.Form.entity.field
        { id = "vaccineName"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.vaccinesNameLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.commonValidation.alphaNumericDashSpace
                    , R10.Form.validation.maxLength 500
                    ]
                }
        }


vaccineSpotEntity : R10.Language.Language -> R10.Form.Entity
vaccineSpotEntity lang =
    R10.Form.entity.field
        { id = "injectionSpot"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.vaccinesInjectionSpotLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.commonValidation.alphaNumericDashSpace
                    , R10.Form.validation.maxLength 500
                    ]
                }
        }


vaccineNumberEntity : R10.Language.Language -> R10.Form.Entity
vaccineNumberEntity lang =
    R10.Form.entity.field
        { id = "serialNumber"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.vaccinesSerialNbLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.commonValidation.alphaNumericDashSpace
                    , R10.Form.validation.maxLength 500
                    ]
                }
        }


vaccineVeterinaryEntity : R10.Language.Language -> R10.Form.Entity
vaccineVeterinaryEntity lang =
    R10.Form.entity.field
        { id = "veterinary"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textPlain
        , label = THealthRecord.vaccinesVeterinaryLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.commonValidation.alphaNumericDashSpace
                    , R10.Form.validation.maxLength 500
                    ]
                }
        }


notesEntity : R10.Language.Language -> R10.Form.Entity
notesEntity lang =
    R10.Form.entity.field
        { id = "notes"
        , idDom = Nothing
        , type_ = R10.FormTypes.inputField.textMultiline
        , label = THealthRecord.notesLabel lang True
        , helperText = Nothing
        , requiredLabel = Nothing
        , validationSpecs =
            Just
                { showPassedValidationMessages = False
                , hidePassedValidationStyle = False
                , validationIcon = R10.FormTypes.NoIcon
                , validation =
                    [ R10.Form.commonValidation.alphaNumericDashSpace
                    ]
                }
        }
