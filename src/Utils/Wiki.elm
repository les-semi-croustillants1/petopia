module Utils.Wiki exposing (..)

import R10.FormTypes
import R10.Language
import Utils.Translations.Wiki as TWiki


listAnimals : List String
listAnimals =
    [ "Cat"
    , "Dog"
    , "Fish"
    ]


listCatBreeds : List String
listCatBreeds =
    [ "Siamese"
    , "Persan"
    ]


listDogBreeds : List String
listDogBreeds =
    [ "Labrador"
    , "Dachshund"
    ]


listFishBreeds : List String
listFishBreeds =
    [ "Goldfish"
    , "Clownfish"
    ]


animalOptions : R10.Language.Language -> List R10.FormTypes.FieldOption
animalOptions lang =
    [ { value = "Cat", label = TWiki.catLabel lang }
    , { value = "Dog", label = TWiki.dogLabel lang }
    , { value = "Fish", label = TWiki.fishLabel lang }
    ]


breedOptions : R10.Language.Language -> String -> List R10.FormTypes.FieldOption
breedOptions lang animalValue =
    if animalValue == "Dog" then
        [ { value = "Labrador", label = TWiki.labradorLabel lang }
        , { value = "Dachshund", label = TWiki.dachshundLabel lang }
        ]

    else if animalValue == "Cat" then
        [ { value = "Siamese", label = TWiki.siameseLabel lang }
        , { value = "Persan", label = TWiki.persanLabel lang }
        ]

    else if animalValue == "Fish" then
        [ { value = "Goldfish", label = TWiki.goldFishLabel lang }
        , { value = "Clownfish", label = TWiki.clownFishLabel lang }
        ]

    else
        []


mapFunction : R10.FormTypes.FieldOption -> String
mapFunction fieldOption =
    fieldOption.value


getTlAnimal : R10.Language.Language -> String -> String
getTlAnimal lang animal =
    let
        list =
            animalOptions lang

        listSingle =
            List.filter (\e -> e.value == animal) list

        content =
            Maybe.withDefault { value = "", label = "" } <| List.head listSingle

        label =
            content.label
    in
    label


getTlBreed : R10.Language.Language -> String -> String -> String
getTlBreed lang animal breed =
    let
        list =
            breedOptions lang animal

        listSingle =
            List.filter (\e -> e.value == breed) list

        content =
            Maybe.withDefault { value = "", label = "" } <| List.head listSingle

        label =
            content.label
    in
    label
