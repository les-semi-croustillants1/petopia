module Utils.Translations exposing
    ( Translations
    , projectName
    , translate
    )

import R10.Language exposing (Language)


type alias Translations =
    { key : String
    , en : String
    , fr : String
    }


r10Translations : Translations -> R10.Language.Translations
r10Translations texts =
    { key = texts.key
    , en_us = texts.en
    , fr_fr = texts.fr
    , es_es = ""
    , de_de = ""
    , ja_jp = ""
    , zh_tw = ""
    }


translate : Language -> Translations -> String
translate lang texts =
    R10.Language.select lang <| r10Translations texts


projectName : Language -> String
projectName lang =
    translate
        lang
        { key = "Project Name"
        , en = "Petopia"
        , fr = "Petopia"
        }
