module Utils.Colors exposing (headerIcon)

import Element


headerIcon : Element.Color
headerIcon =
    Element.rgb 1 1 1
