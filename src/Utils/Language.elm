module Utils.Language exposing
    ( fromLanguagewithDefault
    , fromPath
    , fromString
    , fromStringwithDefault
    , supportedLanguageList
    )

import Maybe
import R10.Language


supportedLanguageList : List R10.Language.Language
supportedLanguageList =
    [ R10.Language.EN_US
    , R10.Language.FR_FR
    ]


fromString : String -> Maybe R10.Language.Language
fromString lang =
    if String.length lang == 2 then
        R10.Language.fromString supportedLanguageList lang

    else
        Nothing


fromStringwithDefault : String -> R10.Language.Language
fromStringwithDefault lang =
    Maybe.withDefault R10.Language.default (fromString lang)


fromLanguagewithDefault : Maybe R10.Language.Language -> R10.Language.Language
fromLanguagewithDefault lang =
    Maybe.withDefault R10.Language.default lang


fromPath : String -> Maybe R10.Language.Language
fromPath path =
    let
        decomposedPath : List String
        decomposedPath =
            String.split "/" path
    in
    case decomposedPath of
        _ :: lang :: _ ->
            fromString lang

        _ ->
            Nothing
