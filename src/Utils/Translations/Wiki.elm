module Utils.Translations.Wiki exposing (..)

import R10.Language exposing (Language)
import Utils.Translations exposing (..)


wikiTitle : Language -> String
wikiTitle lang =
    translate
        lang
        { key = "Wiki Page Title"
        , en = "Wiki"
        , fr = "Wiki"
        }


catLabel : Language -> String
catLabel lang =
    translate
        lang
        { key = "Cat Label"
        , en = "Cat"
        , fr = "Chat"
        }


dogLabel : Language -> String
dogLabel lang =
    translate
        lang
        { key = "Dog Label"
        , en = "Dog"
        , fr = "Chien"
        }


fishLabel : Language -> String
fishLabel lang =
    translate
        lang
        { key = "Fish Label"
        , en = "Fish"
        , fr = "Poisson"
        }


siameseLabel : Language -> String
siameseLabel lang =
    translate
        lang
        { key = "Siamese Label"
        , en = "Siamese"
        , fr = "Siamois"
        }


persanLabel : Language -> String
persanLabel lang =
    translate
        lang
        { key = "Persan Label"
        , en = "Persan"
        , fr = "Persan"
        }


labradorLabel : Language -> String
labradorLabel lang =
    translate
        lang
        { key = "Labrador Label"
        , en = "Labrador"
        , fr = "Labrador"
        }


dachshundLabel : Language -> String
dachshundLabel lang =
    translate
        lang
        { key = "Dachshund Label"
        , en = "Dachshund"
        , fr = "Teckel"
        }


clownFishLabel : Language -> String
clownFishLabel lang =
    translate
        lang
        { key = "Clownfish Label"
        , en = "Clownfish"
        , fr = "Poisson clown"
        }


goldFishLabel : Language -> String
goldFishLabel lang =
    translate
        lang
        { key = "Goldfish Label"
        , en = "Goldfish"
        , fr = "Poisson rouge"
        }


dropDownAnimalLabel : Language -> String
dropDownAnimalLabel lang =
    translate
        lang
        { key = "DropDown Animal Label"
        , en = "Animal"
        , fr = "Animal"
        }


dropDownBreedLabel : Language -> String
dropDownBreedLabel lang =
    translate
        lang
        { key = "DropDown Breed Label"
        , en = "Breed"
        , fr = "Race"
        }
