module Utils.Translations.Home exposing (..)

import R10.Language exposing (Language)
import Utils.Translations exposing (..)


homepageTitle : Language -> String
homepageTitle lang =
    translate
        lang
        { key = "Title Homepage"
        , en = "Homepage"
        , fr = "Page d'accueil"
        }


catchPhrase : Language -> String
catchPhrase lang =
    translate
        lang
        { key = "Catch phrase"
        , en = "The new Utopia for your pets"
        , fr = "La nouvelle Utopie pour vos animaux"
        }


secondSectionTitle : Language -> String
secondSectionTitle lang =
    translate
        lang
        { key = "Second Section Title"
        , en = "Latest Update!"
        , fr = "Dernière mise à jour!"
        }


secondSectionBody : Language -> String
secondSectionBody lang =
    translate
        lang
        { key = "Second Section Body"
        , en = "Added a new feature where you can easily manage your pets' health booklet."
        , fr = "Ajout d'une nouvelle fonctionnalité qui vous permet de gérer facilement les carnets de santé de vos animaux de compagnie."
        }


thirdSectionTitle : Language -> String
thirdSectionTitle lang =
    translate
        lang
        { key = "Third Section Title"
        , en = "The aim"
        , fr = "L'objectif"
        }


thirdSectionBody : Language -> String
thirdSectionBody lang =
    translate
        lang
        { key = "Third Section Body"
        , en = "This website is designed to help pets owners to take care of their pets. We offer you reliable information about pets and reliable contacts for pets."
        , fr = "Ce site est conçu pour aider les propriétaires d'animaux de compagnie à prendre soin de leurs animaux. Nous vous proposons des informations fiables sur les animaux de compagnie et des contacts fiables pour les animaux de compagnie."
        }


fourthSectionTitle : Language -> String
fourthSectionTitle lang =
    translate
        lang
        { key = "Fourth Section Title"
        , en = "Team's presentation"
        , fr = "Présentation de l'équipe"
        }


fourthSectionBody : Language -> String
fourthSectionBody lang =
    translate
        lang
        { key = "Fourth Section Body"
        , en = "This utopia has been created thanks to these 8 incredible guys"
        , fr = "Cette utopie a été créée grâce à ces 8 hommes incroyables"
        }


developerRole : Language -> String
developerRole lang =
    translate
        lang
        { key = "Developer role"
        , en = "Developer"
        , fr = "Développeur"
        }


architectRole : Language -> String
architectRole lang =
    translate
        lang
        { key = "Architect Role"
        , en = "Architect"
        , fr = "Architecte"
        }


managerRole : Language -> String
managerRole lang =
    translate
        lang
        { key = "Manager Role"
        , en = "Project manager"
        , fr = "Chef de projet"
        }


communicationManagerRole : Language -> String
communicationManagerRole lang =
    translate
        lang
        { key = "Communication Manager Role"
        , en = "Communication manager"
        , fr = "Responsable communication"
        }
