module Utils.Translations.Eula exposing (..)

import R10.Language exposing (Language)
import Utils.Translations exposing (..)


eulaTitle : Language -> String
eulaTitle lang =
    translate
        lang
        { key = "EULA Page Title"
        , en = "EULA"
        , fr = "CGU"
        }
