module Utils.Translations.NotFound exposing (..)

import R10.Language exposing (Language)
import Utils.Translations exposing (..)


notFoundTitle : Language -> String
notFoundTitle lang =
    translate
        lang
        { key = "404 Page Title"
        , en = "Page not found"
        , fr = "Page introuvable"
        }


notFoundContent : Language -> String
notFoundContent lang =
    translate
        lang
        { key = "404 Page Content"
        , en = "Error 404 - Page not found!"
        , fr = "Erreur 404 - Page introuvable !"
        }
