module Utils.Translations.HealthRecord exposing (..)

import R10.Language as Language exposing (Language)
import Utils.Translations exposing (..)


healthRecordTitle : Language -> String
healthRecordTitle lang =
    translate
        lang
        { key = "Health Record Page Title"
        , en = "Health Record"
        , fr = "Carnet de santé"
        }


generalInfoSectionTitle : Language -> String
generalInfoSectionTitle lang =
    translate
        lang
        { key = "General Info Section Title"
        , en = "General Info"
        , fr = "Informations Générales"
        }


identificationSectionTitle : Language -> String
identificationSectionTitle lang =
    translate
        lang
        { key = "Identification Section Title"
        , en = "Identification"
        , fr = "Identification"
        }


medicalInfoSectionTitle : Language -> String
medicalInfoSectionTitle lang =
    translate
        lang
        { key = "Medical Info Section Title"
        , en = "Medical Info"
        , fr = "Informations Médicales"
        }


vaccinesSectionTitle : Language -> String
vaccinesSectionTitle lang =
    translate
        lang
        { key = "Vaccines Section Title"
        , en = "Vaccines"
        , fr = "Vaccins"
        }


notesSectionTitle : Language -> String
notesSectionTitle lang =
    translate
        lang
        { key = "Notes Section Title"
        , en = "Notes"
        , fr = "Notes"
        }


colon : Language -> Bool -> String
colon lang formLabel =
    if formLabel then
        ""

    else
        case lang of
            Language.EN_US ->
                ": "

            Language.FR_FR ->
                " : "

            _ ->
                ""


nameLabel : Language -> Bool -> String
nameLabel lang formLabel =
    translate
        lang
        { key = "Name Label"
        , en = "Name" ++ colon lang formLabel
        , fr = "Nom" ++ colon lang formLabel
        }


dobLabel : Language -> Bool -> String
dobLabel lang formLabel =
    translate
        lang
        { key = "DoB Label"
        , en = "Date of birth" ++ colon lang formLabel
        , fr = "Date de naissance" ++ colon lang formLabel
        }


dateFieldHelper : Language -> String
dateFieldHelper lang =
    translate
        lang
        { key = "Date Field Helper"
        , en = "YYYY-MM-DD"
        , fr = "AAAA-MM-JJ"
        }


breedLabel : Language -> Bool -> String
breedLabel lang formLabel =
    translate
        lang
        { key = "Breed Label"
        , en = "Breed" ++ colon lang formLabel
        , fr = "Race" ++ colon lang formLabel
        }


colorLabel : Language -> Bool -> String
colorLabel lang formLabel =
    translate
        lang
        { key = "Color Label"
        , en = "Color" ++ colon lang formLabel
        , fr = "Couleur" ++ colon lang formLabel
        }


sexLabel : Language -> Bool -> String
sexLabel lang formLabel =
    translate
        lang
        { key = "Sex Label"
        , en = "Sex" ++ colon lang formLabel
        , fr = "Sexe" ++ colon lang formLabel
        }


sexMaleLabel : Language -> String
sexMaleLabel lang =
    translate
        lang
        { key = "Sex Male Label"
        , en = "Male"
        , fr = "Mâle"
        }


sexFemaleLabel : Language -> String
sexFemaleLabel lang =
    translate
        lang
        { key = "Sex Female Label"
        , en = "Female"
        , fr = "Femelle"
        }


tattooNbLabel : Language -> Bool -> String
tattooNbLabel lang formLabel =
    translate
        lang
        { key = "Tattoo nb Label"
        , en = "Tattoo Nb" ++ colon lang formLabel
        , fr = "No de tatouage" ++ colon lang formLabel
        }


electonicIdLabel : Language -> Bool -> String
electonicIdLabel lang formLabel =
    translate
        lang
        { key = "Electronic Id Label"
        , en = "Electronic Id" ++ colon lang formLabel
        , fr = "Id puce" ++ colon lang formLabel
        }


diseasesLabel : Language -> Bool -> String
diseasesLabel lang formLabel =
    translate
        lang
        { key = "Diseases Label"
        , en = "Diseases" ++ colon lang formLabel
        , fr = "Maladies" ++ colon lang formLabel
        }


treatmentsLabel : Language -> Bool -> String
treatmentsLabel lang formLabel =
    translate
        lang
        { key = "Treatments Label"
        , en = "Treatments" ++ colon lang formLabel
        , fr = "Traitements" ++ colon lang formLabel
        }


vaccinesDateLabel : Language -> Bool -> String
vaccinesDateLabel lang formLabel =
    translate
        lang
        { key = "Vaccines Date Label"
        , en = "Date" ++ colon lang formLabel
        , fr = "Date" ++ colon lang formLabel
        }


vaccinesNameLabel : Language -> Bool -> String
vaccinesNameLabel lang formLabel =
    translate
        lang
        { key = "Vaccines Name Label"
        , en = "Name" ++ colon lang formLabel
        , fr = "Nom" ++ colon lang formLabel
        }


vaccinesInjectionSpotLabel : Language -> Bool -> String
vaccinesInjectionSpotLabel lang formLabel =
    translate
        lang
        { key = "Vaccines Injection Spot Label"
        , en = "Injection spot" ++ colon lang formLabel
        , fr = "Localisation de l'injection" ++ colon lang formLabel
        }


vaccinesSerialNbLabel : Language -> Bool -> String
vaccinesSerialNbLabel lang formLabel =
    translate
        lang
        { key = "Vaccines Serial Nb Label"
        , en = "Serial number" ++ colon lang formLabel
        , fr = "Numéro de série" ++ colon lang formLabel
        }


vaccinesVeterinaryLabel : Language -> Bool -> String
vaccinesVeterinaryLabel lang formLabel =
    translate
        lang
        { key = "Vaccines Veterinary Label"
        , en = "Veterinary" ++ colon lang formLabel
        , fr = "Vétérinaire" ++ colon lang formLabel
        }


notesLabel : Language -> Bool -> String
notesLabel lang formLabel =
    translate
        lang
        { key = "Notes Label"
        , en = "Notes" ++ colon lang formLabel
        , fr = "Notes" ++ colon lang formLabel
        }


deleteAnimalButton : Language -> String
deleteAnimalButton lang =
    translate
        lang
        { key = "Delete Animal Button"
        , en = "Delete Animal"
        , fr = "Supprimer cet animal"
        }


saveFormAnimalButton : Language -> String
saveFormAnimalButton lang =
    translate
        lang
        { key = "Save Form Animal Button"
        , en = "Save"
        , fr = "Enregistrer"
        }


editFormAnimalButton : Language -> String
editFormAnimalButton lang =
    translate
        lang
        { key = "Edit Form Animal Button"
        , en = "Edit"
        , fr = "Editer"
        }


popUpDeleteAnimalHeader : Language -> String
popUpDeleteAnimalHeader lang =
    translate
        lang
        { key = "Pop Up Delete Animal Header"
        , en = "Caution!"
        , fr = "Attention !"
        }


popUpDeleteAnimalBody : Language -> String
popUpDeleteAnimalBody lang =
    translate
        lang
        { key = "Pop Up Delete Animal Body"
        , en = "Are you sure that you want to delete this animal?"
        , fr = "Etes-vous sûr de vouloir supprimer cet animal ?"
        }


popUpDeleteAnimalYesButton : Language -> String
popUpDeleteAnimalYesButton lang =
    translate
        lang
        { key = "Pop Up Delete Animal Yes Button"
        , en = "Yes"
        , fr = "Oui"
        }
