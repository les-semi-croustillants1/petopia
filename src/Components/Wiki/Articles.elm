module Components.Wiki.Articles exposing (view)

import Color
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import R10.Color.AttrsBorder
import R10.Color.AttrsFont
import R10.Color.Utils
import R10.Mode
import R10.Theme


view : R10.Theme.Theme -> Element msg
view theme =
    column
        -- articles
        [ width (fillPortion 1 |> minimum 200)
        , height fill
        , Border.widthEach { right = 0, left = 3, top = 0, bottom = 0 }
        , R10.Color.AttrsBorder.normal theme
        , padding 15
        ]
        [ row
            -- articles
            [ centerX
            , height <| px 30
            , padding 50
            ]
            [ el
                [ R10.Color.AttrsFont.normal theme ]
                (text "Articles en lien")
            ]
        , row
            -- text "article"
            [ width fill ]
            [ column
                [ spacing 30
                , width fill
                ]
                (viewArticles theme)

            -- return a list
            ]
        ]


viewArticles : R10.Theme.Theme -> List (Element msg)
viewArticles theme =
    List.map
        (viewArticle theme)
        [ ( "Titre1", "blablabla1" )
        , ( "Titre2", "blablabla2" )
        , ( "Titre3", "blablabla3" )
        , ( "Titre4", "blablabla4" )
        , ( "Titre4", "blablabla4" )
        , ( "Titre4", "blablabla4" )
        , ( "Titre4", "blablabla4" )
        ]


viewArticle : R10.Theme.Theme -> ( String, String ) -> Element msg
viewArticle theme article =
    let
        ( title, content ) =
            article
    in
    column
        ([ width fill
         , padding 10
         , spacing 10
         , Border.rounded 5
         , Border.width 1
         , Background.color <| R10.Color.Utils.fromColorColor Color.darkGray
         ]
            ++ (if R10.Mode.isLight theme.mode then
                    [ Border.shadow
                        { offset = ( 1, 1 )
                        , blur = 5
                        , size = 2
                        , color = R10.Color.Utils.fromColorColor Color.darkGray
                        }
                    ]

                else
                    [ R10.Color.AttrsBorder.normal theme ]
               )
        )
        [ el
            [ padding 10 ]
            (text title)
        , el
            []
            (text content)
        ]
