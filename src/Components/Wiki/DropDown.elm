module Components.Wiki.DropDown exposing (Model, Msg, init, update, view)

import Components.Dropdown
import Element exposing (..)
import Element.Border as Border
import R10.Button
import R10.Color.AttrsFont
import R10.Form
import R10.Language
import R10.Libu
import R10.Theme
import Spa.Generated.Route as Route
import Utils.Translations.Wiki as WikiTranslate
import Utils.Wiki as Wiki


type alias Model =
    { dropDownBreed : R10.Form.SingleModel
    , dropDownAnimal : R10.Form.SingleModel
    }


type SingleFieldType
    = Animal
    | Breed


init : Model
init =
    { dropDownBreed = R10.Form.initSingle
    , dropDownAnimal = R10.Form.initSingle
    }


type Msg
    = SingleMsg SingleFieldType R10.Form.SingleMsg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SingleMsg formType singleMsg ->
            case formType of
                Animal ->
                    let
                        single =
                            R10.Form.updateSingle singleMsg model.dropDownAnimal

                        singleModel =
                            Tuple.first single

                        cmd =
                            Cmd.map (SingleMsg formType) (Tuple.second single)
                    in
                    ( { model | dropDownAnimal = singleModel }, cmd )

                Breed ->
                    let
                        single =
                            R10.Form.updateSingle singleMsg model.dropDownBreed

                        singleModel =
                            Tuple.first single

                        cmd =
                            Cmd.map (SingleMsg formType) (Tuple.second single)
                    in
                    ( { model | dropDownBreed = singleModel }, cmd )


view :
    { dropdowns : Model
    , lang : R10.Language.Language
    , theme : R10.Theme.Theme
    }
    -> Element Msg
view options =
    row
        [ spacing 30
        , centerX
        , Border.widthEach { bottom = 1, top = 0, left = 0, right = 0 }
        , paddingXY 0 10
        , R10.Color.AttrsFont.normal options.theme
        ]
        [ Components.Dropdown.view
            { attrs = []
            , singleModel = options.dropdowns.dropDownAnimal
            , label = WikiTranslate.dropDownAnimalLabel options.lang
            , disabled = False
            , key = "animal"
            , theme = options.theme
            , fieldOptions = Wiki.animalOptions options.lang
            , msg = SingleMsg Animal
            , valid = Just True
            }
        , Components.Dropdown.view
            { attrs = []
            , singleModel = options.dropdowns.dropDownBreed
            , label = WikiTranslate.dropDownBreedLabel options.lang
            , disabled =
                if List.member options.dropdowns.dropDownAnimal.value (List.map Wiki.mapFunction (Wiki.animalOptions options.lang)) then
                    False

                else
                    True
            , key = "breed"
            , theme = options.theme
            , fieldOptions = Wiki.breedOptions options.lang options.dropdowns.dropDownAnimal.value
            , msg = SingleMsg Breed
            , valid = Just True
            }
        , R10.Button.primary [ width shrink ]
            { label = text "Search"
            , libu =
                R10.Libu.Li
                    (Route.toString <|
                        Route.Lang_String__Wiki__Animal_String__Breed_String
                            { lang = R10.Language.toStringShort options.lang
                            , animal = options.dropdowns.dropDownAnimal.value
                            , breed = options.dropdowns.dropDownBreed.value
                            }
                    )
            , theme = options.theme
            }
        ]
