module Components.Dropdown exposing (view)

import Element exposing (..)
import R10.Form
import R10.FormTypes
import R10.Theme


view :
    { attrs : List (Attribute msg)
    , singleModel : R10.Form.SingleModel
    , label : String
    , disabled : Bool
    , key : String
    , theme : R10.Theme.Theme
    , fieldOptions : List R10.FormTypes.FieldOption
    , msg : R10.Form.SingleMsg -> msg
    , valid : Maybe Bool
    }
    -> Element msg
view options =
    R10.Form.viewSingle
        options.attrs
        options.singleModel
        { label = options.label
        , helperText = Nothing
        , disabled = options.disabled
        , requiredLabel = Nothing
        , key = options.key
        , style = R10.Form.style.outlined
        , palette = R10.Form.themeToPalette options.theme
        , singleType = R10.FormTypes.SingleSelect
        , fieldOptions = options.fieldOptions
        , toMsg = options.msg
        , valid = options.valid
        }
