{
    description = "Petopia";
    inputs = {
        unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    };
    outputs = inputs:
        let 
            system = "x86_64-linux";
            pkgs = inputs.unstable.legacyPackages.${system};
        in {
            devShell."${system}" = pkgs.mkShell {
                buildInputs = with pkgs.elmPackages; [
                    elm
                    elm-format
                    elm-instrument
                    elm-json
                    elm-analyse
                    elm-coverage
                    elm-doc-preview
                    elm-live
                    elm-optimize-level-2
                    elm-review
                    elm-test
                    elm-upgrade
                    elm-verify-examples
                    elm-xref
                    elm-language-server
                ];
            };
        };
}