# Petopia

## Where to find the website
You can find the project [here](https://petopia-app.netlify.app/).  

## How to contribute

### Installation

#### Windows
You should first [install Node.js](https://nodejs.org/en/).  
When it's done, you can clone the project, either using git bash with:  
`git clone https://gitlab.com/les-semi-croustillants1/petopia`  
or using a software like GitKraken with the following address:  
`https://gitlab.com/les-semi-croustillants1/petopia`

#### Linux (Ubuntu / Debian)
Open the terminal and execute the following commands:
```
sudo apt install npm nodejs
git clone https://gitlab.com/les-semi-croustillants1/petopia
```

### Editing
Run the following command in the project folder:
```
npm start
```
You can now edit any file on the project.  
You will be able to see your modifications on <localhost:8000> after saving you work.

### Using SSH keys
Working with SSH keys for easy push and pull:  
SSH key should be generated with `ssh-keygen`.  
Generated public ssh key (default location is `~/.ssh/id_rsa.pub`) should be added to your gitlab profile via the settings.  
Don't forget to change the origin of the project with this command:  
```
git remote set-url origin git@gitlab.com:les-semi-croustillants1/petopia.git
```
You can watch [this video](https://youtu.be/mNtQ55quG9M) if you encounter any issues.
