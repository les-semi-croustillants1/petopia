module Example exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)


suite : Test
suite =
  describe "This is a test example"
    [ test "This is an expect true test" <|
        \_ -> Expect.true "Always true" True
    ]
